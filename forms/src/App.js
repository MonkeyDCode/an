import React, { Component } from 'react';
import FormularioSimple from './components/formularioSimple'
import axios from 'axios';
export default class App extends Component {
  constructor(){
    super();
    axios.get('https://jsonplaceholder.typicode.com/users')
      .then(({data})=>console.log(data))
    axios.post('https://jsonplaceholder.typicode.com/users',{
      name:'test name',
      username:'testUsername'
    }).then(({data})=>console.log(data))
    /** PETICIONES AJAX 
    fetch('https://jsonplaceholder.typicode.com/users')
      .then ( x => x.json() )
      .then ( x => console.log(x))
    fetch('https://jsonplaceholder.typicode.com/users',{
      method:'POST',
      headers:{
        'Content-Type':'application/json',
      },
      body:JSON.stringify({
        name:'test Name',
        username:''
      })
    }).then( x => x.json())
      .then(x =>console.log(x))
   */
  }
  render() {
    return (
      <div className="App">
        <FormularioSimple />
      </div>
    );
  }
  
}

