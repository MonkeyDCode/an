import React, { Component } from 'react';
import Cabecera from './componentes/cabecera';
import P from './componentes/parrafo';
import './App.css';

class App extends Component {
  state={
    miau : 'miau'
  }
  manejaClick = text =>{
    console.log(text)
  }

  cambiarEstado = () =>{
    this.setState({miau:'Hola Mundo'})
  }

  render() {
    const {miau} = this.state;
    const text = 'Bienvenido al curso'
    return (
      <div className="App">
        <Cabecera miau={miau} 
          manejaClick={this.manejaClick}/>
        <P onClick={this.cambiarEstado}>
          {text}
        </P>
      </div>
      
    );
  }
}

export default App;
