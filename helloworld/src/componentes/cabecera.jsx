import React,{Component} from 'react';
import logo from '../logo.svg';

/**
 *   background-color: #282c34;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  font-size: calc(10px + 2vmin);
  color: white;
 */
const styles={
    header: ({backgroundColor}) => ({
      backgroundColor,
      minHeight: '100vh',
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      fontSize: 'calc(10px + 2vmin)',
      color: 'white'
    })
  }

export default class Cabecera extends Component{
    state = {
        backgroundColor : '#282c34'
    }
    manejaClick = () =>{
        const {miau, manejaClick} = this.props;
        manejaClick(miau);
    }
    colorHeader = () =>{
        this.setState({backgroundColor:'#555'})
    }
    render(){
        const {miau} = this.props;
        const {backgroundColor} = this.state;
        return(
            <header onClick = {this.colorHeader} style={styles.header({backgroundColor})}>
                <img onClick={this.manejaClick} src={logo} className="App-logo" alt="logo" />
                <p>
                    {miau}
                </p>
                <a className="App-link" href="https://reactjs.org"
                target="_blank" rel="noopener noreferrer" >
                  Aprende sobre react
                </a>
            </header>
        );

    }
}
