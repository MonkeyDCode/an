const Hapi = require('hapi');


const server = new Hapi.Server({
    host:'localhost',
    port:3020
});



server.route({
    method : 'GET',
    path: '/',
    handler : function(request, h){
        return ('Hello world')
    }
})


server.start(function(err){
    if(err){
        throw err;
    }

    console.log('server started')
})


