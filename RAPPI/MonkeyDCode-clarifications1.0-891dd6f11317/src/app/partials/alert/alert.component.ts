import { Component, OnInit } from '@angular/core';
import { DataProxyService } from './../../services/data-proxy.service';
import { Router } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

// Services
import { NavigationService } from './../../services/navigation.service';

/**
 * Modal windows
 *
 * @class AlertComponent
 */
@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  providers: [
    NavigationService
  ]
})
export class AlertComponent implements OnInit {
  
  public type = '';
/**
 *Creates an instance of AlertComponent.
 * @param {DataProxyService} dataProxyService
 * @param {BsModalRef} bsModalRef
 * @param {NavigationService} navigationService
 * @param {Router} router
 * @param {BsModalService} modalService
 * @memberof AlertComponent
 */
constructor(
    private dataProxyService: DataProxyService,
    private bsModalRef: BsModalRef,
    private navigationService: NavigationService,
    private router: Router,
    private modalService: BsModalService,
  ) {}

  /**
   * Angular Lifecycle hook: When the component it is initialized
   *
   * @returns {void}
   */
  public ngOnInit(): void {
    document.body.style.overflow = 'hidden';
  }

  /**
   * Close the modal window
   *
   * @returns {void}
   */
  public cancelExecuteBlock(): void {
    this.accept();
  }

  /**
   * When the user clicks on accept button
   *
   * @returns {void}
   */
  public accept(): void {
    this.navigationService.goToRoot();
  }

  /**
   * redirect to the questions screen
   * 
   *
   * @memberof AlertComponent
   */
  public goToQuestions(){
    for (let i = 1; i <= this.modalService.getModalsCount(); i++) {
      this.modalService.hide(i);
    };
    document.getElementById('body').style.removeProperty('overflow');
    document.getElementById('body').classList.remove("modal-open");
    this.router.navigate(['questionnaire']);
  }

  /**
   * Close the modal window
   *
   * @returns {void}
   *
   *
   * @memberof AlertComponent
   */
  public cancelBlocker(): void {
    this.dataProxyService.getQuestionsStatusService().emit('cancelBlocker');

  }

  /**
   * Close the modal window
   *
   * @returns {void}
   */
  public executeBlocker(): void {
    this.dataProxyService.getQuestionsStatusService().emit('executeCardBlock');
  }
}
