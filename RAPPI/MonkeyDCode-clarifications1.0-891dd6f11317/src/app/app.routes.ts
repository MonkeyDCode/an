import { Routes } from '@angular/router';
import { PreloaderComponent,
         WelcomeComponent,
         QuestionnaireComponent,
         SummaryComponent,
         ResultComponent,
         HistoryComponent,
         NoContentComponent,
         NoConnectionComponent,
         LockedComponent } from './pages';
import { DataResolver } from './app.resolver';

//TDD imports components
import {WelcomeTddComponent,QuestionnaireTddComponent,AlertsTddComponent,
  CardTddComponent,FooterTddComponent,SteperTddComponent,LockedTddComponent,
  ResultTddComponent,SpinnerTddComponent,SummaryTddComponent,PreloaderTddComponent} from './pages/tdd/';

export const ROUTES: Routes = [
  { path: '', component: PreloaderComponent },
  { path: 'tdc', component: PreloaderComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'questionnaire', component: QuestionnaireComponent },
  { path: 'summary', component: SummaryComponent },
  { path: 'result', component: ResultComponent },
  { path: 'history', component: HistoryComponent },
  { path: 'no-content', component: NoContentComponent },
  { path: 'no-connection', component: NoConnectionComponent },
  { path: 'locked', component: LockedComponent },
  { path: 'tdd',component:PreloaderTddComponent},
  { path: 'welcomeTDD', component: WelcomeTddComponent },
  { path: 'questionnaireTDD', component: QuestionnaireTddComponent },
  { path: 'resultTDD', component: ResultTddComponent },
  { path: 'summaryTDD', component: SummaryTddComponent },
  { path: '**', component: NoContentComponent }
];
