/**
 * Multifolio object used on the clarification request.
 *
 * @export
 * @class MultifolioModel
 */
export class MultifolioModel {
    public TxrCodigoCom: string;
    public TxrComercio: string;
    public TxrDivisa: string;
    public TxrFecha: string;
    public TxrHrTxr: string;
    public TxrModoEntrada: string;
    public TxrMonto: string;
    public TxrMovExtracto: string;
    public TxrNumExtracto: string;
    public TxrReferencia: string;
    public TxrSucursalAp: string;
    public TxrTipoFactura: string;
    public TxrPAN: string;
    public TxrClacon:string;
    public TxrRefEmisor:string;
 }
 
