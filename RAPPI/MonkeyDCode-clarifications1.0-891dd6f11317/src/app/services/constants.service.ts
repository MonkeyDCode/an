import { Injectable } from '@angular/core';

/**
 * Constants used in all of the application
 *
 *
 * @export
 * @class ConstantsService
 */
@Injectable()
export class ConstantsService {
  /**
   *Enviroment array initialized as empty
   *
   * @type {Array<{
   *     env: string,
   *     url: string,
   *     rating: string,
   *   }>}
   * @memberof ConstantsService
   */
  public ENVIROMENT: Array<{
    env: string,
    url: string,
    rating: string,
  }> = [];

  /**
   * Adds each enviroment to the array
   *Creates an instance of ConstantsService.
   * @memberof ConstantsService
   */
  constructor() {
    // Development
    this.ENVIROMENT.push({
      env: 'dev',
      url: 'https://mxgestaclar-gateway-mxgestaclar-dev.appls.cto2.paas.gsnetcloud.corp/',
      rating: 'https://stars-score-mxgestaclar-dev.appls.cto2.paas.gsnetcloud.corp'
    });
    // Pre-production
    this.ENVIROMENT.push({
      env: 'pre',
      url: 'https://mxgestaclar-gateway-mxgestaclar-pre.appls.cto2.paas.gsnetcloud.corp/',
      rating: 'https://stars-score-mxgestaclar-pre.appls.cto2.paas.gsnetcloud.corp'
    });
    // Production
    this.ENVIROMENT.push({
      env: 'pro',
      url: 'https://mxgestaclar-gateway-mxgestaclar-pro.appls.cto2.paas.gsnetcloud.com/',
      rating: 'https://stars-score-mxgestaclar-pro.appls.cto2.paas.gsnetcloud.corp'
    });
  }
}
