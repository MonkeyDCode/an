import { Injectable, Inject } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { PersistenceService, StorageType } from 'angular-persistence';
import { Observable } from 'rxjs/Observable';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import 'rxjs/add/operator/map';
import * as _ from 'lodash';

// Constants
import { ConstantsService } from  './constants.service';
import { TaggingService } from  './tagging.service';
// Modal component
import { AlertComponent } from './../partials/alert';
import { DataProxyService } from './data-proxy.service';

/**
 * rest service like injectable class
 *
 * @class DataService
 */
@Injectable()
export class DataService {
  private timeout: number;
  private MDW_URL = '';
  private REST_URL = '';
  private CATALOGS_URL = '';
  private USER_REST_URL = '';
  private WALLET_REST_URL = '';
  private FOLIOS_REST_URL = '';
  private SSO_TOKEN_URL = '';
  private RATING_URL = '';
  private CHECK_LOCK_CARDS = '';
  private DEBIT_URL = '';
  // Modal reference
  private modalRef: BsModalRef;

  constructor (
    private http: Http,
    private constantsService: ConstantsService,
    private modalService: BsModalService,
    private dataProxyService: DataProxyService,
    private persistenceService: PersistenceService,
    private taggingService: TaggingService
  ) {
    persistenceService.defineProperty(
      this, 'RATING_URL', 'ratingUrlProperty',
      {type: StorageType.MEMORY});
  }
/**
 *vaidates and sets the endpoints
 *
 * @param {string} env
 * @memberof DataService
 */
public setUris(env: string) {
    // Search the enviroment
    const enviroment = _.find(this.constantsService.ENVIROMENT, (item) => {
      return item.env === env;
    });
    // Define the API URL
    if (process.env.NODE_ENV === 'development') {
      this.MDW_URL = enviroment.url;
    } else {
      this.MDW_URL = '/';
    }
    this.REST_URL = this.MDW_URL.concat('v1/api-backend');
    this.CATALOGS_URL = this.MDW_URL.concat('v1/api-catalogs');
    this.USER_REST_URL = this.MDW_URL.concat('v1/api-user');
    this.WALLET_REST_URL = this.MDW_URL.concat('v1/wallet');
    this.FOLIOS_REST_URL = this.MDW_URL.concat('v1/folios');
    this.SSO_TOKEN_URL = this.MDW_URL.concat('v1/api-sso');
    this.CHECK_LOCK_CARDS = this.MDW_URL.concat('v1/api-check-lock-cards');
    this.DEBIT_URL = this.MDW_URL.concat('v1/debitcards');
    this.setRatingUrl(enviroment.rating);
  }

  /**
   * Set the rating URL
   *
   * @param value {string}
   */
  public setRatingUrl(value: string): void {
    this.RATING_URL = value;
  }

  /**
   * Get the rating URL
   */
  public getRatingUrl(): string {
    return this.RATING_URL;
  }

  public getData(url: string): Observable<any> {
    return this.http.get(url)
                    .map((data: Response) => this.extractData(data));
  }

  public restRequest(
      url: string,
      type: String = 'GET',
      params: any = {
        meta : 'post'
      },
      service = 'backend',
      tokenOauth: String = '',
      clientId?: string 
  ): Observable<any> {
    let apiUrl: String = this.getApiURL(service);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let tokentemp = 'Bearer ' + tokenOauth;
    headers.append('Authorization', tokentemp);
    clientId===undefined ? headers.append('KeyValue',this.dataProxyService.getIdToken()) :  headers.append('KeyValue',clientId);   
    let options = new RequestOptions({
      headers,
      withCredentials: true
    });
    if (type === 'GET') {
      return this.http.get(apiUrl.concat(url),options)
                      .map((data: Response) => this.extractData(data))
                      .catch((error: any) => Observable.throw(this.handleError(error, service)))
                      .share();;
    } else {
      return this.http.post(apiUrl.concat(url), params, options)
                      .timeout(90000)
                      .map((data: Response) => this.extractData(data))
                      .catch((error: any) => Observable.throw(this.handleError(error, service)))
                      .share();;
    }
  }

  /**
   * Get movements from the endpoint
   *
   * @param ID {string}
   * @returns {any}
   */
  public getMovements(ID: string): any {
    const params = {
      extract: ID
    };
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('Authorization', 'Bearer ' + this.dataProxyService.getAccessToken());
    headers.append('KeyValue',this.dataProxyService.getIdToken());
    let options = new RequestOptions({
      headers,
      withCredentials: true
    });
    let promise = new Promise((resolve, reject) => {
      let apiUrl = `${this.USER_REST_URL}/moves/`;
      this.http.post(apiUrl, params, options)
        .toPromise()
        .then(
          (response) => { // Success
            resolve(response.json());
          },
          (error) => { // Error
            reject(error);
          }
        );
    });
    return promise;
  }

  /**
   * Dummy request
   *
   * @param url {string}
   */
  public dummyRequest(url: string) {
    return this.http.get(url)
                    .map((data: Response) => this.extractData(data))
                    .catch((error: any) => Observable.throw(this.handleError(error)));
  }

  /**
   * Handle error
   *
   * @param error {Response | any}
   */
  public handleError (error: Response | any, type: string = '') {
    let options: any = {
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    if (type !== 'catalogs') {
      this.modalRef = this.modalService.show(
        AlertComponent,
        options,
      );
      if (error.name === 'TimeoutError') {
        this.modalRef.content.type = 'timeoutError';
      } else if(error.name ==="Error"){
        this.modalRef.content.type = 'cancelExecuteBlock';
      } else {
        this.modalRef.content.type = 'servicesError';
      }
    }
    // GA - Tealium
    const dataLayer = {
      17: `step-${type}`,
      27: this.modalRef.content.type,
    };
    this.taggingService.uTagView(dataLayer);
    this.taggingService.send();
    let errMsg = error.message ? error.message : error.toString();
    Promise.reject(errMsg);
    return errMsg;
  }

  /**
   * Get the API URL according to the service
   *
   * @param service {string}
   * @returns {string}
   */
  public getApiURL(service: string): string {
    let apiUrl = '';
    switch(service) {
      case '':
        apiUrl = this.REST_URL;
      break;
      case 'config':
      case 'token':
        apiUrl = './';
      break;
      case 'user':
        apiUrl = this.USER_REST_URL;
      break;
      case 'catalogs':
        apiUrl = this.CATALOGS_URL;
      break;
      case 'wallet':
        apiUrl = this.WALLET_REST_URL;
      break;
      default:
        apiUrl = this.getApiURL2(service);
    }
    return apiUrl;
  }

    /**
   * Get the API URL according to the service 2
   *
   * @param service {string}
   * @returns {string}
   */
  private getApiURL2(service:string):string{
    let url = '';
    switch(service){
      case 'folios':
      url = this.FOLIOS_REST_URL;
      break;
      case 'sso_token':
        url = this.SSO_TOKEN_URL;
      break;
      case 'rating':
        url = this.RATING_URL;
      break;
      case 'check_lock':
        url = this.CHECK_LOCK_CARDS;
      break;
      case 'debit':
        url = this.DEBIT_URL;
      break;
      case 'dev_url':
        url = '';
      break;
      default:
        url = this.MDW_URL;
    }
    return url;
  }

  /**
   * Extract data
   *
   * @param res {Response}
   * @returns {any}
   */
  private extractData(res: Response): any {
    let body = res.json();
    return body;
  }
}
