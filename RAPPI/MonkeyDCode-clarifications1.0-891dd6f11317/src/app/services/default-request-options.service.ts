import { Injectable } from '@angular/core';
import { BaseRequestOptions, RequestOptions } from '@angular/http';
/**
 *the headers in the rest request
 *
 * @export
 * @class DefaultRequestOptions
 * @extends {BaseRequestOptions}
 */
@Injectable()
export class DefaultRequestOptions extends BaseRequestOptions {

  constructor() {
    super();

    // Set the default 'Content-Type' header
    this.headers.set('Content-Type', 'application/json');
  }
}
/**
 * Provider in all the http frequests
 * 
 */
export const requestOptionsProvider = { provide: RequestOptions, useClass: DefaultRequestOptions };
