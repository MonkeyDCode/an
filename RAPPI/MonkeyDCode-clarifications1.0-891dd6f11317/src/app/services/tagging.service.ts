import { Injectable } from '@angular/core';
import * as _ from 'lodash';

/**
 * Window interface for the TypeScript compiler
 */
declare global {
    interface Window {
        ga: any;
        utag_data: any;
        dataLayer: any;
        utag: any;
    }
}

/**
 * Tagging:
 *
 * @class TaggingService
 */
@Injectable()
export class TaggingService {
    private customDimensions: Array<{index: string, label: string}> = [];
    private events: Array<{index: string, label: string}> = [];

    constructor() {
        // Google Analytics custom dimensions
        this.customDimensions = [
            {index: '1', label: 'section'},
            {index: '2', label: 'subsection1'},
            {index: '3', label: 'subsection2'},
            {index: '4', label: 'subsection3'},
            {index: '5', label: 'page'},
            {index: '6', label: 'screenView'},
            {index: '7', label: 'userID'},
            {index: '8', label: 'movement_id'},
            {index: '9', label: 'got_card'},
            {index: '10', label: 'commerce_interactions'},
            {index: '11', label: 'clarification_reason'},
            {index: '12', label: 'current_location'},
            {index: '13', label: 'federal_entity'},
            {index: '14', label: 'card_incident'},
            {index: '15', label: 'loss_date'},
            {index: '16', label: 'amount'},
            {index: '17', label: 'funnel'},
            {index: '18', label: 'process'},
            {index: '19', label: 'card_number'},
            {index: '20', label: 'search_movements'},
            {index: '21', label: 'filter_movements'},
            {index: '22', label: 'filter_movements-cut_date'},
            {index: '23', label: 'selected_movements'},
            {index: '24', label: 'delete_selected_movements'},
            {index: '25', label: 'select_movement_icon'},
            {index: '26', label: 'select_movement_description'},
            {index: '27', label: 'show_alert'},
        ];
        // Tealium events
        this.events = [
            {index: '1', label: 'interaction_category'},
            {index: '2', label: 'interaction_action'},
            {index: '3', label: 'interaction_label'}
        ];
    }

    /**
     * Add data to the Google Tag and Tealium data layers
     *
     * @returns {void}
     */
    public gtag(...args: any[]): void {
        _.assign(window.dataLayer, args);
        _.assign(window.utag_data, args);
    }

    /**
     * Set value for the User ID.
     *
     * @returns {void}
     */
    public setUserID(value: string): void {
        window.ga('set', 'userId', value);
        this.setDimenson('7', value);
    }

    /**
     * Set value for custom dimension at index "idx".
     *
     * @param index {string}
     * @param value {string}
     * @returns {void}
     */
    public setDimenson(index: string, value: string): void {
        window.ga('set', 'dimension' + index, value);
        this.updateDataLayer(index, value);
    }

    /**
     * Set the pagename concatenating the values of other tags
     *
     * @returns {void}
     */
    public setPageName(): void {
        let pageName = '';
        const tags: String[] = [
            'section',
            'subsection1',
            'subsection2',
            'funnel',
        ];
        tags.forEach((tag) => {
            pageName += '/' + _.get(window.utag_data, tag);
        });
        window.utag_data['pagename'] = pageName;
    }

    /**
     * Add the key: value pair to the object
     *
     * @param index {string}
     * @param value {string}
     * @returns {string}
     */
    public updateDataLayer(index: string, value: string): string {
        const item = _.find(this.customDimensions, {index});
        window.dataLayer[item.label] = value;
        window.utag_data[item.label] = value;
        return item.label;
    }

    /**
     * Track the user actions
     *
     * @param args {any[]}
     * @returns {void}
     */
    public uTagView(data: any): void {
        let dataView: Object = {};
        _.each(data, (value, key) => {
            this.setDimenson(key, value);
            dataView[this.updateDataLayer(key, value)] = value;
        });
        this.gtag(dataView);
        if (!_.isUndefined(window.utag)) {
            window.utag.view(dataView);
        }
    }

    /**
     * Send event
     *
     * @param category {string}
     * @param action {string}
     * @param label {string}
     *
     * @returns {void}
     */
    public sendEvent(category: string, action: string, label: string): void {
        window.ga('send', 'event', category, action, label, {
            'dimension1': '', // Custom Dimension 1
            'dimension2': 'Some value 2', // Custom Dimension 2
            'hitCallback': () => {}
        });
    }

    /**
     * Send the custom dimension value with a pageview hit.
     *
     * @returns {void}
     */
    public send(): void {
        window.ga('send', 'pageview');
    }
}
