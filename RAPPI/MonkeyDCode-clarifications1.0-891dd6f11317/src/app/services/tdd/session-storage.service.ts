import { Injectable,Inject } from '@angular/core';
import { SESSION_STORAGE, StorageService } from 'angular-webstorage-service';


/**
 * llave para almacenar el token
 */
const TOKEN = 'sessionid';
/**
 * servicio que se encarga de consultar y guardar los datos de la sesion
 *
 * @export
 * @class SessionStorageService
 */
@Injectable()
export class SessionStorageService {
    /**
     *Creates an instance of SessionStorageService.
    * @param {StorageService} storage
    * @memberof SessionStorageService
    */
    constructor(@Inject(SESSION_STORAGE) private storage:StorageService) { }

    /**
     * guarda en sesion el valor {val} mediante la llave {key}
     *
     * @param {*} key
     * @param {*} val
     * @memberof SessionStorageService
     */
    saveInLocal(key, val): void {
        this.storage.set(key, val);
    }

    /**
     * consulta de sesion la llave {key} y la regresa
     *
     * @param {*} key
     * @returns {*}
     * @memberof SessionStorageService
     */
    getFromLocal(key): any {
        return this.storage.get(key);
    }

    /**
     * guarda el token de la aplicacion en sesion
     *
     * @param {*} token
     * @memberof SessionStorageService
     */
    saveToken(token){
        this.storage.set(TOKEN, token);
    }

    /**
     * consulta de sesion el token y lo regresa
     *
     * @returns
     * @memberof SessionStorageService
     */
    getToken(){
        return this.storage.get(TOKEN);
    }

}