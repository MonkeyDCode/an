import { Injectable, state } from '@angular/core';
import { HttpHeaders} from '@angular/common/http';
import * as _ from 'lodash';
import * as moment from 'moment';
import {SessionStorageService} from './session-storage.service';
import { DataService } from './../../services/data.service';
import { DataProxyService } from './../../services/data-proxy.service';
import { MoveModel, QuestionsModel, LoaderModel,
    BlockModel, CreditCardFullDataModel, ResponseModel, AnswersQuestionsModel,
    MultifolioModel } from './../../models';

//Services

@Injectable()
export class UtilsTddService {
    private responseDAO: ResponseModel;
    private userData = this.storage.getFromLocal('userdata');
    private CONSTANTS : any = this.constants();

    constructor(
      private storage: SessionStorageService,
      private dataService: DataService,
      public dataProxyService: DataProxyService
    ) {

    }

  scrolltop(){
    setTimeout(()=>{
        window.scrollTo(0,0);
    },100)
        
  }

  getChannelApp(){
    if(this.dataProxyService.getChannel()=='default'){
        return  "SuperMóvil";
      }else{
        return  "SuperWallet";
      }
  }
  generateViewQuestions(motive:any,description:string,date:any,location:string):Array<any>{
    let questions=[];
    switch(Number(motive.id)){
        case 0:
            break;
        case 1:
        case 2:
        case 3:
        case 4:
        case 5:
        case 6:
        case 7:
            questions = this.getInteractionView(motive,description);
            break;
        case 8:
            break;
        case 9:
        case 10:
        default:
            break;

    }
    questions.push({key:'UBICACIÓN ACTUAL',value:location});
    return questions;
  }

  getInteractionView(motive:any, description:string):Array<any>{
    let questions = [{key:'TARJETA EN SU PODER',value:'SÍ'},
                    {key:'INTERACTUÓ CON EL COMERCIO',value:'SÍ'},
                    {key:'MOTIVO',value:motive.description.toUpperCase()},
                    {key:'DESCRIPCIÓN',value:description.toUpperCase()}];
    return questions;
  }

    /**
   * Format Date.
   *
   * @private
   * @param {string} v
   * @returns {string}
   * @memberof WelcomeComponent
   */
  private formatDate(v: string): string {
    return moment(v, 'YYYY-MM-DD')
      .format('YYYY-MM-DD[T06:00:00+00:00]').toString();
  }

    /**
   * Format hour.
   *
   * @private
   * @param {string} h
   * @returns {string}
   * @memberof SummaryComponent
   */
  private formatHour(h: string): string {
    if (h === '' || h === null || typeof h === 'undefined') {
      return '00:00:00';
    } else {
      return h.replace('.', ':');
    }
  }


      /**
   * Get the Multifolio.
   *
   * @private
   * @returns
   * @memberof SummaryComponent
   */
  private getMultifolioModel(moves) {
    let multifolio = [];
    let ix = 0;
    _.each(moves, (item: MoveModel) => {
      let objmultifolio = new MultifolioModel();
      objmultifolio.TxrCodigoCom = item.txrCodigoCom;
      objmultifolio.TxrComercio = item.txrComercio;
      objmultifolio.TxrDivisa = item.txrDivisa;
      objmultifolio.TxrFecha = item.txrFecha;
      objmultifolio.TxrHrTxr = this.formatHour(item.txrHrTxr);
      objmultifolio.TxrModoEntrada = item.txrModoEntrada;
      objmultifolio.TxrMonto = item.txrMonto;
      objmultifolio.TxrMovExtracto = item.txrMovExtracto;
      objmultifolio.TxrNumExtracto = item.txrNumExtracto;
      objmultifolio.TxrReferencia = item.txrReferencia;
      objmultifolio.TxrSucursalAp = item.txrSucursalAp;
      objmultifolio.TxrTipoFactura = item.txrTipoFactura;
      objmultifolio.TxrPAN = item.txrPAN;
      objmultifolio.TxrClacon = item.txrClacon; //TODO: 
      objmultifolio.TxrRefEmisor = item.txrRefEmisor;//TODO
      multifolio.push(objmultifolio); ix++;
    });
    return multifolio;
  }


  generateMove(v,m){
    let dateStr = moment(v.acctTrnInfo.stmtDt, 'YYYY-MM-DD').format('MM-DD-YYYY').toString();
    let periodStr: string =  moment(dateStr, 'MM-DD-YYYY').format('MMMM YYYY');
    let fecha = this.formatDate(v.acctTrnInfo.stmtDt);
    let parsedStrDate  = dateStr.split('-');
    let typedDate = new Date(Number(parsedStrDate[2]),Number(parsedStrDate[0])-1,Number(parsedStrDate[1]));
    let nmove: MoveModel= new MoveModel(
        v.acctTrnId,
        v.acctTrnInfo.trnType.desc,
        v.acctTrnInfo.totalCurAmt.amt.toString(),
        dateStr,
        periodStr,
        v.acctTrnInfo.networkTrnData.merchNum,
        v.acctTrnInfo.networkTrnData.merchName,
        v.acctTrnInfo.origCurAmt.curCode.curCodeValue,
        fecha,
        v.acctTrnInfo.trnTime,
        v.acctTrnInfo.networkTrnData.posEntryCapability,
        v.acctTrnInfo.totalCurAmt.amt,
        this.storage.getFromLocal('ccdata').cardAcctRelRec[0].cardAcctRelInfo.cardRef
            .cardRec.cardInfo.fiData.branchIdent,
        v.acctTrnId,
        m,
        v.acctTrnInfo.salesSlipRefNum,//dfef
        v.acctTrnInfo.trnType.trnTypeCode,
        v.acctTrnInfo.cardRef.cardInfo.cardNum,
        typedDate,
        v.acctTrnInfo.clacon,//TxrClacon
        v.acctTrnInfo.refEmisor//TxrRefEmisor
    )
    return nmove;
    
  }

  generateSMObject(){
      return {wvrinboxrn:{
        ApellidosNomEmp:this.storage.getFromLocal('userdata').name,
        Apto:0,//vacio
        BUC:this.storage.getFromLocal('buc'),
        Categoria:'TARJETA DE DEBITO',//this.storage.getFromLocal('category'),
        CompaniaCelular:'',//vacio
        Cuenta:this.storage.getFromLocal('ccdata')['cardAcctRelRec'][0].cardAcctRelInfo.acctRef.acctRec.acctId,
        Descripcion:this.storage.getFromLocal('additionaldata').description,
        Email:'',//back
        EntidadFed:this.storage.getFromLocal('additionaldata').location.toString(),
        FechaRobada:this.storage.getFromLocal('additionaldata').lostdate,
        Marca:this.storage.getFromLocal('userdata').cardBrand,
        Movil:'',//back
        Nombre:this.storage.getFromLocal('userdata').name,
        PAN:this.storage.getFromLocal('pan'),
        Producto:this.storage.getFromLocal('userdata').cardName,
        Razon:'',//vacio
        Segmento:this.storage.getFromLocal('segment'),
        Subcategoria:this.storage.getFromLocal('subcategory'),
        SucursalAltair:'',//BAck
        VisaCarta:'false',
        cuestionario:this.storage.getFromLocal('questionnaire'),
        multifolio:this.getMultifolioModel( this.storage.getFromLocal('multifolio'))
        }}
  }
    /**
     * Return files needed according to the reason.
     *
     * @param {string} reason
     * @returns {Array<any>}
     * @memberof UtilsTddService
     */
    getRequirementsResult(reason:string) : Array<any>{
        const requirementsResult = {
            receiptCorrectAmount : [{value:this.CONSTANTS.LABELS.RECEIPT_CORRECT_AMOUNT_DOC}],
            receiptRecognized : [{value:this.CONSTANTS.LABELS.RECEIPT_RECOGNIZED_DOC}],
            receiptPayment : [{value:this.CONSTANTS.LABELS.RECEIPT_PAYMENT_DOC}],
            receiptReturn : [{value:this.CONSTANTS.LABELS.RECEIPT_RETURN_DOC}],
            letterVoucher : [
                {value:this.CONSTANTS.LABELS.LETTER_VOUCHER_DOC}, 
                {value:this.CONSTANTS.LABELS.LETTER_VOUCHER_COPY},  
            ],
            voucherCancel : [{value:this.CONSTANTS.LABELS.VOUCHER_CANCEL_DOC}],
        }
        return requirementsResult[reason];   
    }

    handleServiceManagerRequest(serviceResponse: any): any {
        this.validateSMHandler(serviceResponse);
        let response: any = new ResponseModel();
        let dateC = '';
        if (this.storage.getFromLocal('dummy')) {
            response.setResult(301); // DUMMY
        }
        const currentDate = moment().format('DD/MMM/YYYY HH:mm').toString();
        response.setCurrentDate(currentDate.split('.').join(''));
        if(serviceResponse.Messages){
            let nationalFolios = [];
            let internationalFolios = [];
            let payment = false;
            let date = moment();
            for(let item of serviceResponse.Messages){
                if (this.getPayment(item) === 'true') payment = true;
                const internationalFolio = this.getFolioDate(item, 'INTERNACIONAL');
                const nationalFolio = this.getFolioDate(item, 'NACIONAL');
                let temporaldate = this.greaterDateValue(internationalFolio, nationalFolio);
                date = this.greaterDateValue(date, temporaldate);
                let tempFoliosQ = this.getFolio(item, 'Internacional');
                let tempArrayF = this.extractFolio(tempFoliosQ);
                internationalFolios = this.concatArray(internationalFolios,tempArrayF);
                tempFoliosQ = this.getFolio(item, 'Nacional');
                tempArrayF = this.extractFolio(tempFoliosQ);
                nationalFolios = this.concatArray(nationalFolios,tempArrayF);
            }
            response.setInternationalFolio(internationalFolios);
            response.setNationalFolio(nationalFolios);
            response.setGreaterDate(date);
            response.setPayment(payment);
            response.setAmount(serviceResponse.wvrinboxrn.monto * 1);
            response.setResult(301);
        }
        response.setDateCommitment(dateC);
        response.setTotalAmount(serviceResponse.wvrinboxrn.monto * 1);
        response.setName(this.userData.name);
        response.setVisaCard(serviceResponse.wvrinboxrn.VisaCarta);
        response.setOldCard(this.userData.cardNumber);
        return response;
    }

    validateSMHandler(serviceResponse: any){
        // Check if the reponse is an error
        if (!_.isUndefined(serviceResponse.codigoMensaje)) {
            if (serviceResponse.codigoMensaje === 'MSG-001') {
            this.dataService.handleError({ name: serviceResponse.mensaje });
            }
        }
        if(serviceResponse.status==="Error"){
            this.dataService.handleError({name: serviceResponse.status});
        }
    }

    getPayment(serviceResponseItem: string[], ): any {
        const find = _.find(serviceResponseItem, (item: any) => { return item.match(new RegExp(`^Abono: `)); });
        if (!_.isUndefined(find)) {
            return (find.split(': ')[1] !== 'undefined') ? find.split(': ')[1] : null;
        }
        return null;
    }

    getFolioDate(serviceResponseItem: string[], v: string): any {
        const find = _.find(serviceResponseItem, (item: any) => {
          return item.match(new RegExp(`^${v} next_breach ==> `));
        });
        if (!_.isUndefined(find)) {
          return moment(find.split('==> ')[1], 'DD/MM/YYYY HH::mm:ss');
        }
        return null;
    }

    greaterDateValue(international, national): any {
        let greater = null;
        if (international && national) {
          greater = (international.isAfter(national)) ? international : national;
        } else if (international) {
          greater = international;
        } else if (national) {
          greater = national;
        }
        return greater;
    }

    getFolio(serviceResponseItem: string[], folioType: string): any {
        const find = _.find(serviceResponseItem, (item: any) => {
          return item.match(new RegExp(`^${folioType}: `));
        });
        if (!_.isUndefined(find)) {
          if(find.split(': ')[1].includes(' ')){
            let a =0;
            let tempFolio='';
            let arrayFolio = find.split(': ')[1].split(' ');
            if(arrayFolio[0]!== 'undefined' && arrayFolio[1]!== 'undefined'){
              return `${arrayFolio[0]}|${arrayFolio[1]}`;
            }else{
              arrayFolio[0]!== 'undefined'? tempFolio= tempFolio+ arrayFolio[0] :  a+=1;
              arrayFolio[1]!== 'undefined'? tempFolio= tempFolio+ arrayFolio[1] :  a+=1;
              tempFolio === '' ? tempFolio =  null : a+=1;
              return tempFolio;
            }
          }
          return (find.split(': ')[1] !== 'undefined') ? find.split(': ')[1] : null;
        }
        return null;
    }

    extractFolio(tempFolio){
        let tempArray =[];
        if(tempFolio !== null){
          tempFolio.includes('|') ? tempArray = tempFolio.split('|') : tempArray.push(tempFolio);
        }
        return tempArray;
    }

    concatArray(origin,toConcat){
        toConcat.forEach(function(element) {
          origin.push(element);
        });
        return origin;
    }

    public getHeadersRequest(){
       const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': 'Bearer '+ this.storage.getFromLocal('app-access'),
              'KeyValue': this.storage.getFromLocal('client') || 'keyvalue'
            })
        };
        return httpOptions;
    }
     /**
     * Constants collection
     *
     * @returns {object}
     * @memberof UtilsTddService
     */
    constants() : object {
        return {
            STORAGE: {
                CHANNEL: 'chanel',
                SM_RESPONSE: 'SMResponse',
                QUESTION_ID: 'questionId',
            },
            LABELS: {
                PAYMENT_AMOUNT: 'Monto abonado',
                PAYMENT_DESCRIPTION: 'Hemos realizado un abono temporal a su tarjeta',
                CLARIFICATION_AMOUNT: 'Monto de la aclaración',
                CLARIFICATION_REGISTER: 'Hemos dado de alta su aclaración',
                SUPERMOBILE: 'SuperMóvil',
                SUPERWALLET: 'SuperWallet',
                NATIONAL_FOLIO: 'FOLIO NACIONAL',
                INTERNATIONAL_FOLIO: 'FOLIO INTERNACIONAL',
                FOLIO_NUMBER: 'NÚMERO DE FOLIO',
                RECEIPT_CORRECT_AMOUNT: 'receiptCorrectAmount',
                RECEIPT_RECOGNIZED: 'receiptRecognized',
                RECEIPT_PAYMENT: 'receiptPayment',
                RECEIPT_RETURN: 'receiptReturn',
                LETTER_VOUCHER: 'letterVoucher',
                VOUCHER_CANCEL: 'voucherCancel',
                RECEIPT_CORRECT_AMOUNT_DOC: 'Comprobante de compra con el importe correcto de la transacción realizada.',
                RECEIPT_RECOGNIZED_DOC: 'Comprobante de compra con el importe del (los) cargo(s) que sí reconoce.',
                RECEIPT_PAYMENT_DOC: 'Comprobante que demuestre el pago al comercio.',
                RECEIPT_RETURN_DOC: 'Comprobante de devolución emitido por el comercio.',
                LETTER_VOUCHER_DOC: 'Carta firmada que contenga el detalle de lo sucedido.',
                LETTER_VOUCHER_COPY: 'Copia del comprobante con la fecha de entrega comprometida por el comercio.',
                VOUCHER_CANCEL_DOC: 'Comprobante o folio de cancelación.'
            }
        };
    }

}