import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Router , NavigationEnd } from '@angular/router';
import { NgFor, NgClass, CurrencyPipe } from '@angular/common';
import { ISubscription } from 'rxjs/Subscription';
import { Observable } from 'rxjs/Observable';

import { DataService } from './../../services/data.service';
import { MoveModel,  UserModel, ExtractModel } from './../../models';
import { DataObject } from './../../shared/data.object';
import { DataProxyService } from './../../services/data-proxy.service';
import { TaggingService } from '../../services/tagging.service';
import { NavigationService } from './../../services/navigation.service';

import * as _ from 'lodash';
import * as moment from 'moment';
import * as md5 from 'blueimp-md5';

/**
 *
 *
 * @export
 * @class WelcomeComponent
 * @implements {OnInit}
 * @implements {OnDestroy}
 */
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  providers: [
    DataService,
    DataObject,
    DataProxyService,
    TaggingService,
    NavigationService,
    CurrencyPipe
  ]
})
export class WelcomeComponent implements OnInit, OnDestroy {

  filterApply = false;
  filterName = '';
  sfApply = false;


  public isScrolled = false;
  public isHideNav = false;
  public counter = 0;
  public selectedItems: any = [];
  protected subscription: ISubscription;
  protected movementsSubscription: Array<ISubscription> = [];
  protected dataHandler = Array<MoveModel>();
  protected hiddenMovements = Array<MoveModel>();
  protected selectionHandler = Array<MoveModel>();
  protected rawData: any;
  protected ccData: any;
  protected userData:  UserModel;
  protected headerDate: string;
  protected errorMessage: string;
  protected navToggle = false;
  protected movToggle = false;
  protected isLoading = false;
  protected filterDesc = '';
  protected currentFilter: string;
  protected currentExtract = '';
  protected dateArraysQuantity = 0;
  protected sortedDates: Array<string> = [];
  protected isFirstLoad = true;
  protected bottomReached: boolean;
  protected scrollDisabled = false;
  protected countDown;
  protected count = 3;
  protected isMovementsVisible=false;
  protected selectedFilterValue = -9999;
  protected isFiltered = false;
  private extract=0;

  /**
   * Creates an instance of WelcomeComponent.
   * @param {DataService} dataService
   * @param {DataProxyService} dataProxyService
   * @param {TaggingService} taggingService
   * @param {NavigationService} navigationService
   * @param {Router} router
   * @param {DataObject} dataObject
   * @param {CurrencyPipe} currencyPipe
   * @memberof WelcomeComponent
   */
  constructor(private dataService: DataService,
              public dataProxyService: DataProxyService,
              private taggingService: TaggingService,
              private navigationService: NavigationService,
              private router: Router,
              private dataObject: DataObject,
              private currencyPipe: CurrencyPipe) {
    // Bind methods for the navigation rules
    this.toggleNav = this.toggleNav.bind(this);
    this.toggleMov = this.toggleMov.bind(this);
  }
  /**
   * Loads initial content :: User data and previously selected items.
   *
   * @memberof WelcomeComponent
   */
  public ngOnInit() {
    // Navigation rules
    // this.navigationService.setTitle('Alta de aclaraciones');
    this.isMovementsVisible=false;
    this.navigationService.tapBack('welcome', this.navigationService.goToRoot);

    this.dataService.setUris(this.dataProxyService.getEnviroment());
    if(this.dataProxyService.getDataSelected()){
      this.counter = this.dataProxyService.getDataSelected().length;
    }
    if (this.dataProxyService.getDataSelected()) {
      this.dataHandler = this.dataProxyService.getDataSelected();
      this.selectionHandler = this.dataProxyService.getDataSelected();
    }
    this.userData = this.dataProxyService.getUserData();
    this.ccData   = this.dataProxyService.getCCData();
    this.bottomReached = false;
    if (this.userData.extracts.length > 0) {
      if (this.dataProxyService.getDummyMode()) {
        this.filterExtractsById(this.getLastExtractId(), false, true);
      } else {
        // Get the movements of the first extract
        this.getInitMovements(0);
      }
    }
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0);
    });
    // GA - Tealium
    this.setDataLayer();
  }

  /**
   * Sets the dataLayer for Google Analytics
   *
   * @memberof WelcomeComponent
   * @returns {void}
   */
  public setDataLayer(): void {
    const userID = md5(this.dataProxyService.getBuc(), 'mx-aclaraciones-cs');
    this.taggingService.setUserID(userID);
    const channel = this.dataProxyService.getChannel();
    let section = 'santander supermovil';
    if (channel !== 'default') {
      section = 'santander superwallet';
    }
    const dataLayer = {
      1: section,
      2: 'clarifications',
      3: 'registration',
      4: 'movements list',
      17: 'step-select movements',
      18: 'registration of clarifications',
    };
    this.taggingService.uTagView(dataLayer);
    this.taggingService.setPageName();
    this.taggingService.send();
  }

  /**
   * Method that is called when welcome component is destroyed.
   *
   * @memberof WelcomeComponent
   */
  public ngOnDestroy() {
    // window.removeEventListener('scroll', this.scroll, true);
  }
  /**
   * Submit filter form.
   *
   * @param {Event} $event
   * @memberof WelcomeComponent
   */
  public submitFilterForm($event: Event): void {
    document.getElementById('inlineFormInputGroup').blur();
    this.taggingService.setDimenson('20', 'used');
    this.taggingService.send();
  }
  /**
   * Get the first 20 movements of the credit card ordered by list.
   *
   * @private
   * @param {number} extractIndex
   * @memberof WelcomeComponent
   */
  private getInitMovements(extractIndex: number): void {
    this.extract += 1;
    const ID = this.getExtractId(extractIndex);
    this.currentFilter = ID;
    const movements = this.dataService.getMovements(ID);
    movements.then((response) => {
      if (!_.isUndefined(response.acctTrnRec)) {
        // Check if the response has movements
        if (response.acctTrnRec.length > 0) {
          this.isMovementsVisible=true;
          this.addMovements(extractIndex, response);
          let combinated = [];
          this.hiddenMovements=[];
          _.each(this.userData.extracts, (element) => {
            if (element.moves) {
              _.each(element.moves, (move, index) => {
                if (combinated.length < 20 && index <= 19) {
                  combinated.push(move);
                } else {
                  this.hiddenMovements.push(move);
                  this.hiddenMovements = _.sortBy(this.hiddenMovements, 'typeDate').reverse();
                }
              });
            }
          });
          const groupedMovements = _.groupBy(combinated, 'date');
          this.dataObject.filteredData = groupedMovements;
          this.dataObject.filteredData = this.sortDateKeys();
          this.dateArraysQuantity = Object.keys(this.dataObject.filteredData).length;
          if (combinated.length < 20 || this.extract <= this.dataProxyService.getUserExtracts().length) {
            this.getMoreMovements(extractIndex);
          }
        } else {
          // If the response does not have movements, get the next extract
          this.getMoreMovements(extractIndex);
        }
      }
      // Order the movements by date
      this.dataObject.filteredData = this.sortDateKeys();
    });
  }
  /**
   * Get more movements from the next extract.
   *
   * @private
   * @param {number} extractIndex
   * @memberof WelcomeComponent
   */
  private getMoreMovements(extractIndex: number): void {
    if (!_.isUndefined(this.userData.extracts[extractIndex + 1])) {
      this.getInitMovements(extractIndex + 1);
    }
  }
  /**
   * Add the movements from the response to the extract.
   *
   * @private
   * @param {*} extractIndex
   * @param {*} response
   * @memberof WelcomeComponent
   */
  private addMovements(extractIndex: any, response: any): void {
    let moves: Array<MoveModel> = [];
    _.each(response.acctTrnRec, (v: any) => {
      let dateStr: string = moment(v.acctTrnInfo.stmtDt, 'YYMMDD').format('DD-MM-YYYY');
      let periodStr: string =  moment(dateStr, 'DD-MM-YYYY').format('MMMM YYYY');
      let fecha = this.formatDate(v.acctTrnInfo.stmtDt);
      let parsedStrDate  = dateStr.split('-');
      let typedDate = new Date(Number(parsedStrDate[2]),Number(parsedStrDate[1])-1,Number(parsedStrDate[0]));
      let nmove: MoveModel= new MoveModel(
        v.acctTrnId,
        v.acctTrnInfo.trnType.desc,
        v.acctTrnInfo.totalCurAmt.amt.toString(),
        dateStr,
        periodStr,
        v.acctTrnInfo.networkTrnData.merchNum,
        v.acctTrnInfo.networkTrnData.merchName,
        v.acctTrnInfo.origCurAmt.curCode.curCodeValue,
        fecha,
        v.acctTrnInfo.trnTime,
        v.acctTrnInfo.networkTrnData.posEntryCapability,
        v.acctTrnInfo.totalCurAmt.amt,
        this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef
          .cardRec.cardInfo.fiData.branchIdent,
        v.acctTrnId,
        this.userData.extracts[extractIndex].id,
        v.acctTrnInfo.salesSlipRefNum,
        v.acctTrnInfo.trnType.trnTypeCode,
        v.acctTrnInfo.cardRef.cardInfo.cardNum,
        typedDate
      );
      moves.push(nmove);
    });
    moves = _.sortBy(moves, 'typeDate').reverse();
    this.userData.extracts[extractIndex].moves = moves;
  }
  /**
   * Listen the interaction of the user and validate the native session.
   *
   * @private
   * @param {Event} $event
   * @memberof WelcomeComponent
   */
  @HostListener('window:scroll', ['$event'])
  private onWindowScroll($event: Event): void {
    this.navigationService.validateSession();
    if (window.pageYOffset > 0) {
      this.isScrolled = true;
    } else {
      this.isScrolled = false;
    }
    //const that = this;
    if (!this.scrollDisabled) {
      if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        this.bottomReached = true;
        if (!this.isFirstLoad) {
          this.showNextItems();
        }
       // this.scrollDisabled = true;
        if (this.isFirstLoad) {
          this.isFirstLoad = false;
          this.countDown = Observable.timer(0, 1000)
            .take(this.count)
            .map(() => --this.count );
          this.countDown.subscribe(
            (x) => {},
            (err) => {},
            () => {
              this.scrollDisabled = false;
            }
          );
        }
      }
    }
  }
  /**
   * Sort the array of movements by date.
   *
   * @private
   * @returns {*}
   * @memberof WelcomeComponent
   */
  private sortDateKeys(): any {
    const dates = Object.keys(this.dataObject.filteredData);
    // Format the dates
    let formattedDates: Array<string> = [];
    let sortedDates: Array<string> = [];
    let sortedMovements: Object = {};
    _.each(dates, (date) => {
      formattedDates.push(moment(date, 'DD-MM-YYYY').format('YYYYMMDD'));
    });
    // Sort the formatted dates in descending order
    const sorting = _.sortBy(formattedDates, (o) => {
      return moment(o);
    }).reverse();
    // Format again the sorted dates
    _.each(sorting, (date) => {
      sortedDates.push(moment(date, 'YYYYMMDD').format('DD-MM-YYYY'));
    });
    // Add all movements in the sorted order
    _.each(sortedDates, (date) => {
      sortedMovements[date] = this.dataObject.filteredData[date];
    });
    return sortedMovements;
  }
  /**
   * Add the hidden movements to the list and empty the array after that.
   *
   * @private
   * @memberof WelcomeComponent
   */
  private addHiddenMovements(): void {
    let  movementsDates = Object.keys(this.dataObject.filteredData);
    let temporalHidden = [];
    if(this.hiddenMovements.length>19){
      temporalHidden = this.hiddenMovements.slice(20,this.hiddenMovements.length);
      this.hiddenMovements = this.hiddenMovements.slice(0,19);
    }
    _.each(this.hiddenMovements, (movement) => {
      // Check if the date of the movement are on list
      const dateFound = _.find(movementsDates, (o) => o === movement.date );
      if (_.isUndefined(dateFound)) {
        this.dataObject.filteredData[movement.date] = [];
        this.dataObject.filteredData[movement.date].push(movement);
      } else {
        this.dataObject.filteredData[movement.date].push(movement);
      }
      movementsDates = Object.keys(this.dataObject.filteredData);
    });
    this.dataObject.filteredData = this.sortDateKeys();
    // Clear the hidden movements
    if(temporalHidden.length>0){
      this.hiddenMovements = temporalHidden;
    }else{
      this.hiddenMovements = Array<MoveModel>();
    }
    
  }
  /**
   * Show the hidden movements and the movements of the next extract.
   *
   * @private
   * @memberof WelcomeComponent
   */
  private showNextItems(): void {
    this.scrollDisabled = false;
    this.bottomReached = false;
    // Show the hidden movements
    if (this.hiddenMovements.length > 0) {
      this.addHiddenMovements();
    }
    const decrement: number = Number(this.currentFilter) - 1;
    // Check if the next extract exists
    const decrementIndex: number = this.getExtractIndex(String(decrement));
    if (decrement >= 0 && decrementIndex !== -1) {
      this.filterExtractsById(String(decrement), true, true);
      this.currentFilter = String(decrement);
    }
    this.dateArraysQuantity = Object.keys(this.dataObject.filteredData).length;
    }
  /**
   * Keep Hidden prevents from open in the initial filter.
   *
   * @private
   * @param {string} ID
   * @param {boolean} [keepHidden=false]
   * @param {boolean} [append=false]
   * @param {boolean} [showOnlyExtract=false]
   * @returns {void}
   * @memberof WelcomeComponent
   */
  private filterExtractsById(
    ID: string,
    keepHidden: boolean = false,
    append: boolean = false,
    showOnlyExtract: boolean = false
  ): void {
    if (this.isLoading || typeof ID === 'undefined') { return; }
    const extracts: Array<ExtractModel> = this.userData.extracts;
    const currentSearchIndex: number = this.getExtractIndex(ID);
    this.currentFilter = ID;
    if (!_.isUndefined(extracts[currentSearchIndex]) && currentSearchIndex !== -1) {
      if (!extracts[currentSearchIndex].moves) {
        this.loadExtract(ID);
      } else {
        this.evaluateIndex(extracts, currentSearchIndex, ID, append);
      }
    }
    this.bottomReached = false;
  }
  /**
   * Evaluate index.
   *
   * @private
   * @param {Array<ExtractModel>} a
   * @param {*} b
   * @param {*} c
   * @param {*} d
   * @memberof WelcomeComponent
   */
  private evaluateIndex(a: Array<ExtractModel>, b: any, c: any, d: any) {
    const extracts = a;
    const currentSearchIndex = b;
    const ID = c;
    const append = d;
    if (extracts[currentSearchIndex].moves.length > 0 ) {
      this.currentFilter = ID;
    }
    let idx: number = _.findIndex(extracts, (o: ExtractModel) => { return String(o.id) === String(ID); });
    if (idx > -1) {
      if (this.isFirstLoad) {
        this.loadExtract(ID);
      } else {
        if (append) {
          let combinated = [];
          _.each(extracts, (element) => {
            if (element.moves) {
              combinated = _.unionBy(combinated, element.moves, 'id');
            }
          });
          this.dataObject.filteredData = _.groupBy(combinated, 'date');
          this.dataObject.filteredData = this.sortDateKeys();
        } else {
          this.dataObject.filteredData = _.groupBy(extracts[idx].moves, 'date');
          this.dataObject.filteredData = this.sortDateKeys();
        }
      }
      this.dateArraysQuantity = Object.keys(this.dataObject.filteredData).length;
    } else {
      this.loadExtract(ID);
    }
  }
  /**
   * Show filtered movements.
   *
   * @private
   * @param {string} ID
   * @memberof WelcomeComponent
   */
  private showFilteredMovements(ID: string): void {
    const extracts: Array<ExtractModel> = this.userData.extracts;
    const idx: number = this.getExtractIndex(ID);
    this.isFiltered =false;
    if (!_.isUndefined(extracts[idx]) && idx !== -1) {
      if (extracts[idx].moves) {
        this.dataObject.filteredData = _.groupBy(extracts[idx].moves, 'date');
        this.dataObject.filteredData = this.sortDateKeys();
      } else {
        this.dataObject.filteredData = {};
      }
      this.dateArraysQuantity = Object.keys(this.dataObject.filteredData).length;
    }

    this.filterApply = true;
    this.filterName = document.getElementById('extractLabel-'+ID).textContent;
  }
  /**
   * Check if is the first load.
   *
   * @private
   * @param {*} ID
   * @param {*} idx
   * @memberof WelcomeComponent
   */
  private checkFirstLoad(ID: any, idx: any): void {
    const extracts: Array<ExtractModel> = this.userData.extracts;
    // Get the first 20 movements
    this.dataObject.filteredData = this.getInitialMovements(ID, idx);
    this.dataObject.filteredData = this.sortDateKeys();
    if (Object.keys(this.dataObject.filteredData).length < 20) {
      const nextExtractIndex = Number(idx) + 1;
      if (!_.isUndefined(extracts[nextExtractIndex])) {
        this.checkFirstLoad(extracts[nextExtractIndex].id, nextExtractIndex);
      }
    } else {
      this.isFirstLoad = false;
    }
  }
  /**
   * Get initial movements.
   *
   * @private
   * @param {string} ID
   * @param {number} idx
   * @returns {Object}
   * @memberof WelcomeComponent
   */
  private getInitialMovements(ID: string, idx: number): Object {
    const extracts: Array<ExtractModel> = this.userData.extracts;
    // Get the first 20 movements of the extract
    let aux = _.take(extracts[idx].moves, 20);
    // Check if there are 20 movements
    let difference: number = Math.abs(aux.length - 20);
    let increment: number = idx;
    while (difference > 0) {
      if (!_.isUndefined(extracts[increment])) {
        // If the movements are lower than 20, get the difference from the next extract
        if (!extracts[increment].moves) {
          this.loadExtract(extracts[increment].id);
        }
        difference = Math.abs(aux.length - 20);
        const restOfMovements = _.take(extracts[increment].moves, difference);
        aux = _.unionBy(aux, restOfMovements, 'id');
        increment++;
      } else {
        difference = 0;
      }
    }
    return _.groupBy(aux, 'date');
  }
  /**
   * Load Extract.
   *
   * @private
   * @param {string} ID
   * @memberof WelcomeComponent
   */
  private loadExtract(ID: string) {
    this.isLoading = true;
    this.currentExtract = ID;
    const extractIndex = this.getExtractIndex(ID);
    if (extractIndex !== -1 && !_.isUndefined(this.userData.extracts[extractIndex])) {
      if (!this.userData.extracts[extractIndex].moves) {
        if (this.dataProxyService.getDummyMode()) {
          let dataURI = `assets/data/extract_${ID}.json`;
          this.movementsSubscription[ID] = this.dataService.dummyRequest(dataURI)
            .subscribe((response) => this.processExtractQuery(response, ID));
        } else {
              this.movementsSubscription[ID] = this.dataService
                .restRequest(
                  '/moves/',
                  'POST',
                  {
                    extract: ID
                  },
                  'user',
                  this.dataProxyService.getAccessToken()
                )
                .subscribe(
                  (response) => this.processExtractQuery(response, ID),
                  (error) => this.handleError(error)
                );
        }
      } else {
        this.filterExtractsById(ID, true);
      }
    }
  }
  /**
   * Process Extract Query.
   *
   * @private
   * @param {*} item
   * @param {string} ID
   * @memberof WelcomeComponent
   */
  private processExtractQuery(item: any, ID: string) {
    this.movementsSubscription[ID].unsubscribe();
    moment.locale('es');
    const extractQuery = this.getExtractIndex(this.currentExtract);
    let currentExtract: ExtractModel = this.userData.extracts[extractQuery];
    if (!_.isUndefined(currentExtract)) {
      if (!currentExtract.moves) {
        let moves: Array<MoveModel> = [];
        _.each(item.acctTrnRec, (v: any) => {
          let dateStr: string = moment(v.acctTrnInfo.stmtDt, 'YYMMDD').format('DD-MM-YYYY');
          let periodStr: string =  moment(dateStr, 'DD-MM-YYYY').format('MMMM YYYY');
          let fecha = this.formatDate(v.acctTrnInfo.stmtDt);
          let nmove: MoveModel= new MoveModel(
            v.acctTrnId,
            v.acctTrnInfo.trnType.desc,
            v.acctTrnInfo.totalCurAmt.amt.toString(),
            dateStr,
            periodStr,
            v.acctTrnInfo.networkTrnData.merchNum,
            v.acctTrnInfo.networkTrnData.merchName,
            v.acctTrnInfo.origCurAmt.curCode.curCodeValue,
            fecha,
            v.acctTrnInfo.trnTime,
            v.acctTrnInfo.networkTrnData.posEntryCapability,
            v.acctTrnInfo.totalCurAmt.amt,
            this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef
              .cardRec.cardInfo.fiData.branchIdent,
            v.acctTrnId,
            ID,
            v.acctTrnInfo.salesSlipRefNum,
            v.acctTrnInfo.trnType.trnTypeCode,
            v.acctTrnInfo.cardRef.cardInfo.cardNum
          );
          moves.push(nmove);
        });
        this.userData.extracts[extractQuery].moves = moves;
        this.checkFirstLoad(ID, extractQuery);
      }
    }
    this.isLoading = false;
    if (this.isFirstLoad) {
      this.getInitialMovements(ID, extractQuery);
      this.filterExtractsById(this.currentFilter, false, true);
    } else {
      this.filterExtractsById(this.currentFilter, true, true);
    }
  }
  /**
   * Get extract index.
   *
   * @private
   * @param {string} ID
   * @returns {number}
   * @memberof WelcomeComponent
   */
  private getExtractIndex(ID: string): number {
    let cix: number = _.findIndex(this.userData.extracts, (o: ExtractModel) => {
      return String(o.id) === String(ID);
    });
    return cix;
  }
  /**
   * Get last extract Id.
   *
   * @private
   * @returns {string}
   * @memberof WelcomeComponent
   */
  private getLastExtractId(): string {
    return this.userData.extracts[0].id;
  }
  /**
   * Get extract Id from an index.
   *
   * @private
   * @param {number} index
   * @returns
   * @memberof WelcomeComponent
   */
  private getExtractId(index: number) {
    return this.userData.extracts[index].id;
  }
  /**
   * Get next extract id.
   *
   * @private
   * @param {*} currentIndex
   * @returns {(any | undefined)}
   * @memberof WelcomeComponent
   */
  private getNextExtractId(currentIndex): any | undefined {
    const index = currentIndex + 1;
    if (!_.isUndefined(this.userData.extracts[index])) {
      return this.userData.extracts[index].id;
    }
    return;
  }
  /**
   * Get filter date name.
   *
   * @private
   * @param {string} ID
   * @returns {string}
   * @memberof WelcomeComponent
   */
  private getFilterDateName(ID: string): string {
    if (ID === undefined) {
      return '';
    }
    if (ID === this.getLastExtractId()) {
      if(this.dataProxyService.getChannel()=='default'){
      return 'Periodo actual';
      }else{
      return 'Corte actual';
      }
    } else {
      let idx: number = _.findIndex(this.userData.extracts, { id: ID });
      if (idx > -1) {
        let dateNumber = moment(this.userData.cutoff, 'YYYY-MM-DD', 'es')
        .subtract(idx, 'months');
        let dateNameTo =  dateNumber.format('DD [de] MMMM ');
        dateNumber = dateNumber.add(1,'days');
        dateNumber = dateNumber.subtract(1,'months');
        let dateNameFrom = dateNumber.format('[Del] DD [de] MMMM [al] ');
          return `${dateNameFrom}${dateNameTo}`;
      }
      return '';
    }
  }
  /**
   * Filter By Desc.
   *
   * @private
   * @param {string} evt
   * @memberof WelcomeComponent
   */
  private filterByDesc(evt: string): void {
    this.filterDesc = evt;
    this.dateArraysQuantity = 0;
    // Get the extracts
    const extracts: Array<ExtractModel> = this.userData.extracts;
    const extrIdx: number = this.getExtractIndex(this.currentFilter);
    if (evt === '') {
      if (extrIdx !== -1) {
        if (!_.isUndefined(extracts[extrIdx])) {
          this.dataObject.filteredData = this.filterAllElements(extracts);
          this.dataObject.filteredData = this.sortDateKeys();
          this.dateArraysQuantity =  Object.keys(this.dataObject.filteredData).length;
        } else {
          this.filterExtractsById(this.currentFilter, true);
        }
      } else {
        const filter = Number(this.currentFilter) + 1;
        this.filterExtractsById(String(filter), true);
      }
    } else {
      this.dataObject.filteredData = this.filterAllElements(extracts);
      this.dataObject.filteredData = this.sortDateKeys();
      this.dateArraysQuantity =  Object.keys(this.dataObject.filteredData).length;
    }


    if(evt === ''){
      this.filterApply = false;
      this.filterName = '';
      this.sfApply = false;
    }else{
      this.filterApply = true;
      this.filterName = evt;
    }

  }
   /**
    * Filter show
    *
    * @memberof WelcomeComponent
    */
   showFilterApply() {
    if(this.sfApply){
      this.sfApply = false;
    }else{
      this.sfApply = true;
    }
  }
  /**
   * Filter all elements.
   *
   * @private
   * @param {Array<ExtractModel>} extracts
   * @returns {*}
   * @memberof WelcomeComponent
   */
  private filterAllElements(extracts: Array<ExtractModel>): any {
    let content: any = {};
    _.each(extracts, (element) => {
      if (element.moves && element.moves.length > 0) {
        const tmp = _.filter(element.moves, (o: MoveModel) => {
          return o.txrComercio
                              .toString()
                              .toLowerCase()
                              .indexOf(this.filterDesc.toLowerCase()) > -1;
        });
        _.merge(content, tmp);
      }
    });
    return _.groupBy( content, 'date' );
  }
  /**
   * Highlight the title according to the filter.
   *
   * @private
   * @param {string} title
   * @returns {string}
   * @memberof WelcomeComponent
   */
  private highlightTitle(title: string): string {
    if (!this.filterDesc || this.filterDesc.length === 0) {
      return title;
    }
    return title.replace(new RegExp(this.filterDesc, 'gi'), (match) => {
      return `<span class="highlightText">${match}</span>`;
    });
  }
  /**
   * selection
   *
   * @private
   * @param {*} $evt
   * @param {*} item
   * @param {*} period
   * @memberof WelcomeComponent
   */
  private selection($evt, item, period) {
    if ($evt) {
      const aux: MoveModel= _.find(this.dataHandler, ['id', item.id]);
      if (!aux) {
        item.period = period;
        this.dataHandler.push(item);
      }
    } else {
      _.remove(this.dataHandler, (n) => {
        return n.id === item.id;
      });
    }
    this.dataObject.setSelectedMoves(this.dataHandler);
    
  }
  /**
   * Bypass Array
   *
   * @private
   * @param {*} val
   * @returns
   * @memberof WelcomeComponent
   */
  private bypassArray(val) {
    return Array.from(val);
  }
  /**
   * Check If Selected.
   *
   * @private
   * @param {string} id
   * @param {string} extract
   * @returns
   * @memberof WelcomeComponent
   */
  private checkIfSelected(id: string, extract: string) {
    const idx = _.findIndex(this.dataProxyService.getDataSelected(), (o: MoveModel) => {
      return o.id === id && o.txrNumExtracto === extract;
    });
    if (idx > -1) {
      return true;
    } else {
      return false;
    }
  }
  /**
   * Navigate to the next step.
   *
   * @private
   * @memberof WelcomeComponent
   */
  private nextStep() {
    this.router.navigate(['questionnaire']);
  }
  /**
   * Process data.
   *
   * @private
   * @param {Response} r
   * @memberof WelcomeComponent
   */
  private processData(r: Response) {
    this.subscription.unsubscribe();
    this.rawData = r;
    this.dataObject.filteredData = _.groupBy(this.rawData.transactions[0].moves, 'date');
    this.dataObject.filteredData = this.sortDateKeys();
    this.dataProxyService.setRawData(r);
    this.dateArraysQuantity =  Object.keys(this.dataObject.filteredData).length;
  }
  /**
   * Create range.
   *
   * @private
   * @param {number} limit
   * @returns
   * @memberof WelcomeComponent
   */
  private createRange(limit: number) {
    let items: number[] = [];
    for (let i = 1; i <= limit; i++) {
       items.push(i);
    }
    return items;
  }
  /**
   * Retrieve parsed date.
   *
   * @private
   * @param {*} dateToBeParsed
   * @returns
   * @memberof WelcomeComponent
   */
  private retrieveParsedDate(dateToBeParsed: any) {
    moment.locale('es');
    if(this.dataProxyService.getChannel()==='wallet'){
      return moment(dateToBeParsed, 'DD-MM-YYYY')
        .format('DD MMMM, YYYY');
    }else{
      return moment(dateToBeParsed, 'DD-MM-YYYY')
        .format('dddd DD [de] _MMMM, YYYY');
    }
     
  }
  /**
   * Retrieve date from position at.
   *
   * @private
   * @param {number} n
   * @returns {string}
   * @memberof WelcomeComponent
   */
  private retrieveDateFromPositionAt(n: number): string {
    let obj = Object.keys(this.dataObject.filteredData)[n];
    if(obj === undefined){
      return obj;
    }else{
      return obj.toString();
    }
  }
  /**
   * Retrieve parsed date from position at.
   *
   * @private
   * @param {number} n
   * @returns
   * @memberof WelcomeComponent
   */
  private retrieveParsedDateFromPositionAt(n: number) {
    let obj = Object.keys(this.dataObject.filteredData)[n];
    if(obj === undefined){
      return obj;
    }else{
      return this.retrieveParsedDate(obj.toString());
    }
    
  }
  /**
   * Retrieve parsed date from position at.
   *
   * @private
   * @param {number} n
   * @returns
   * @memberof WelcomeComponent
   */
  private retrieveParsedDateFromPositionAtWallet(n: number) {
    let obj = Object.keys(this.dataObject.filteredData)[n];
    return this.retrieveParsedDate(obj.toString());
  }
  /**
   * Get Movements.
   *
   * @private
   * @param {number} n
   * @returns
   * @memberof WelcomeComponent
   */
  private getMovements(n: number) {
    let obj = Object.keys(this.dataObject.filteredData)[n];
    return this.dataObject.filteredData[obj];
  }
  /**
   * Toggle nav.
   *
   * @private
   * @memberof WelcomeComponent
   */
  private toggleNav(): void {
    // Check if the div is open
    if (!this.navToggle) {
      this.navigationService.tapBack('filters', this.toggleNav);
    } else {
      this.navigationService.tapBack('filters', this.navigationService.goToRoot);
    }
    this.navigationService.validateSession();
    this.navToggle = !this.navToggle;
    this.isHideNav = !this.isHideNav;
    // GA - Tealium
    this.taggingService.setDimenson('21', 'used');
    this.taggingService.send();
  }
  /**
   * Toggle mov.
   *
   * @private
   * @memberof WelcomeComponent
   */
  private toggleMov(): void {
    // Check if the div is open
    if (!this.movToggle) {
      this.navigationService.tapBack('filters', this.toggleMov);
    } else {
      this.navigationService.tapBack('filters', this.navigationService.goToRoot);
    }
    this.navigationService.validateSession();
    this.movToggle = !this.movToggle;
    this.isHideNav = !this.isHideNav;
    if (this.isHideNav){
      document.body.style.overflow = 'hidden';
      this.createMovements();
    } else {
      document.body.style.overflow = 'auto';
      this.selectedItems = [];
    }
    // GA - Tealium
    this.taggingService.setDimenson('23', 'used');
    this.taggingService.send();
  }
  /**
   * Order movements by date.
   *
   * @private
   * @memberof WelcomeComponent
   */
  private createMovements(): void {
    this.selectedItems=[];
    try {
      for (let x = 0; x < this.dataProxyService.getDataSelected().length; x++) {
        this.selectedItemsPush(this.dataHandler[x]);
      }
      this.selectedItems.forEach((element, index) => {
        this.selectedItems[index].data = _.orderBy(element.data, 'txrHrTxr', 'desc');
      });
      this.selectedItems = _.orderBy(this.selectedItems, 'dateKey');
    } catch (error) {
    }
  }
  /**
   * Handle item.
   *
   * @private
   * @param {MoveModel} val
   * @param {string} period
   * @param {string} [type='']
   * @memberof WelcomeComponent
   */
  private handleItem(val: MoveModel, period: string, type: string = '') {
    const idx: any = _.find(this.dataHandler, {id: val.id, txrNumExtracto: val.txrNumExtracto}, 0);
    if (idx) {
      const cindex: number = this.dataHandler.indexOf(idx);
      this.dataHandler.splice(cindex, 1);
    } else {
      const mv: MoveModel= new MoveModel(
        val.id,
        val.desc,
        val.amount.toString(),
        val.date,
        period,
        val.txrCodigoCom,
        val.txrComercio,
        val.txrDivisa,
        val.txrFecha,
        val.txrHrTxr,
        val.txrModoEntrada,
        val.txrMonto,
        this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo.fiData.branchIdent,
        val.txrMovExtracto,
        val.txrNumExtracto,
        val.txrReferencia, 
        val.txrTipoFactura,
        val.txrPAN);
      this.dataHandler.push(mv);
    }
    this.dataProxyService.setDataSource(this.dataHandler);
    this.counter= this.dataHandler.length;
    this.navigationService.validateSession();
    // GA - Tealium
    const dimension = (type === 'icon') ? '25' : '26';
    this.taggingService.setDimenson(dimension, 'used');
    this.taggingService.send();
  }
  /**
   * Add the selected items to the array.
   *
   * @private
   * @param {*} item
   * @memberof WelcomeComponent
   */
  private selectedItemsPush(item: any) {
    let splited = item.date.split('-');
    let dateKey = `${splited[2]}${splited[1]}${splited[0]}`
    if(this.getIndexAt(item.date)===-1){
      this.selectedItems.push({key: item.date, dateKey, data: []});
    }
    this.selectedItems[this.getIndexAt(item.date)].data.push(item);
  }

  /**
   * Select items split.
   *
   * @param {*} item
   * @memberof WelcomeComponent
   */
  selectedItemsSplit(item){
    const idx: any = _.find(this.dataHandler, {id: item.id, txrNumExtracto: item.txrNumExtracto}, 0);
    const cindex: number = this.dataHandler.indexOf(idx);
    this.dataHandler.splice(cindex, 1);
    this.dataProxyService.dataSourceSelected(this.dataHandler);
    let idx2 = _.find(this.selectedItems[this.getIndexAt(item.date)].data,item);
    this.selectedItems[this.getIndexAt(item.date)].data.splice(idx2,1);
    this.counter = this.dataHandler.length;
    this.createMovements();
    // GA - Tealium
    this.taggingService.setDimenson('24', 'used');
    this.taggingService.send();
  }

  /**
   * Get index at.
   *
   * @param {string} key
   * @returns
   * @memberof WelcomeComponent
   */
  getIndexAt(key:string){
    let idx = -1;
    for(let x = 0; x< this.selectedItems.length; x++){
      if(this.selectedItems[x].key===key){
        return x;
      }
    }
    return idx;
  }
  /**
   * Handle error.
   *
   * @private
   * @param {*} error
   * @memberof WelcomeComponent
   */
  private handleError(error: any) {
    //this.router.navigate(['no-connection']);
  }
  /**
   * Format Date.
   *
   * @private
   * @param {string} v
   * @returns {string}
   * @memberof WelcomeComponent
   */
  private formatDate(v: string): string {
    return moment(v, 'YYMMDD')
      .format('YYYY-MM-DD[T06:00:00+00:00]').toString();
  }
   /**
    * Opener Light Box.
    *
    * @param {*} event
    * @param {*} id
    * @memberof WelcomeComponent
    */
   public tooltipOpener(event, id){

    //Define tooltipeText
    let ttText = '';

    if(id){
      ttText = 'Por favor toque “x” para eliminar un movimiento que ya no desee mandar a aclarar.';
    }else{
      ttText = 'Es importante señalar que se pueden aclarar movimientos de hasta 3 meses anteriores a la fecha actual.';
    }

   
    let y = event.clientY;
    let x = event.clientX;
    let tooltip = document.getElementById('tooltip-box');
    let backdrop = document.getElementById('backdrop');
    let tooltipText  = document.getElementById('tooltip-text');
    let flagBorder = document.getElementById('flag-border');
    let flagColor = document.getElementById('flag-color');

    tooltipText.innerHTML = ttText;
    tooltip.style.top = (y+20)+'px';
    tooltip.style.position = 'fixed';
    flagColor.style.left = (x-14)+'px';
    flagBorder.style.left = (x-14)+'px'  ;
    backdrop.classList.remove("tooltip-hide");
    tooltip.classList.remove("tooltip-hide");

  }
  /**
   * Show moves.
   *
   * @param {*} id
   * @memberof WelcomeComponent
   */
  showMoves(id){

    let button= document.getElementById('button'+id);
    let element= document.getElementById('listMoves'+id);

    if(button.classList.contains('active')){
      element.classList.remove('show');
      button.classList.remove('active');
    }else{
      element.classList.add('show');
      button.classList.add('active');
    }
  }

}
