import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import {SessionStorageService} from '../../../services/tdd/session-storage.service';
import {UtilsTddService} from '../../../services/tdd/utils-tdd.service';
import { DataService } from '../../../services/data.service';
import { AlertsMain } from '../alerts-tdd/alertsMain';

/**
 * componente que da el resumen de los movimnientos y respuestas del usuario
 * da de alta la aclaracion
 *
 * @export
 * @class SummaryTddComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-summary-tdd',
  templateUrl: './summary-tdd.component.html',
  providers: [
    DataService  ]
})
export class SummaryTddComponent implements OnInit {
  /** cuestionario de la vista */
  private viewQuestions=[];
  /**canal por el que se accedio a la aplicaicon */
  private chanelType = '';

  /**
   *Creates an instance of SummaryTddComponent.
   * @param {UtilsTddService} utils
   * @param {SessionStorageService} storage
   * @param {DataService} dataService
   * @param {Router} router
   * @param {AlertsMain} alertsMain
   * @memberof SummaryTddComponent
   */
  constructor(private utils:UtilsTddService,private storage: SessionStorageService,
              private dataService: DataService,private router: Router,private alertsMain : AlertsMain) { }
  
  /**
   * navega al inicio de la pagina
   * consulta el cuestionario a mostrar del storage
   * inicializa el servicio de urls
   * consulta el canal de la aplicacion
   *
   * @memberof SummaryTddComponent
   */
  ngOnInit() {
    this.utils.scrolltop();
    this.viewQuestions = this.storage.getFromLocal('viewQuestions');
    this.dataService.setUris(this.storage.getFromLocal('enviroment'));
    this.chanelType = this.storage.getFromLocal('chanel');
    
  }

  /**
   * Hace la consulta del token de la aplicacion para llamar a ejecutar la aclaracion
   * dependiendo del ambiente lo hace en dummy o al endpoint
   * 
   *
   * @private
   * @memberof SummaryTddComponent
   */
  private executeContinue() {
    this.alertsMain.sendMessage(-1,true);
    if (this.storage.getFromLocal('dummy')) {
      this.dataService.dummyRequest('assets/data/token.json')
        .subscribe((response) => {
          this.storage.saveInLocal('app-access',response.access_token);
          this.executeClarification();
        });
    } else {
      this.dataService
        .restRequest('token', 'POST', '', 'token')
        .subscribe(
          (response) => {
            if (response.expires_in <= 10) {
              setTimeout(() => {
                this.executeContinue();
              }, response.expires_in * 1000);
            } else {
              this.storage.saveInLocal('app-access',response.access_token);
              this.executeClarification();
            }
          }
        );
    }
  }


  /**
   * hace el llamado al endpoint para dar de alta la aclaracion
   * dependiendo el ambiente o el modo dummy
   * 
   * hace navegacion a la pantalla de respuesta
   *
   * @memberof SummaryTddComponent
   */
  executeClarification(){
    if(this.storage.getFromLocal('dummy')){
      let subscription = this.dataService.dummyRequest('assets/data/sm-response-tdd.json')
          .subscribe((response) => {
            this.storage.saveInLocal('SMResponse',response);
            this.router.navigate(['resultTDD']);
          });
    }else{
      let SMObject = this.utils.generateSMObject();
      let subscription = this.dataService.restRequest(
        '/clarification/',
        'POST',
        JSON.stringify(SMObject),
        '',
        this.storage.getFromLocal('app-access'),
        this.storage.getFromLocal('client'))
      .subscribe((response) => {
        this.storage.saveInLocal('SMResponse',response);
        this.router.navigate(['resultTDD']);
      });
    }
  }

}
