import { Component, OnInit } from '@angular/core';
/**
 *
 *
 * @export
 * @class LockedTddComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-locked-tdd',
  templateUrl: './locked-tdd.component.html'
})
export class LockedTddComponent implements OnInit {
  /**
   *Creates an instance of LockedTddComponent.
  * @memberof LockedTddComponent
  */
  constructor() { }
  
  /**
   *
   *
   * @memberof LockedTddComponent
   */
  ngOnInit() {
    }

}
