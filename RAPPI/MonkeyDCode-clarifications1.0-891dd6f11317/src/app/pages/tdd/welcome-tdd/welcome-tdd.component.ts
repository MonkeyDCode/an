import { Component, OnInit,ViewChild,forwardRef } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap, Route } from '@angular/router';
import {SessionStorageService} from '../../../services/tdd/session-storage.service';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { DataService } from '../../../services/data.service';
import {UtilsTddService} from '../../../services/tdd/utils-tdd.service';
// Import Tooltips
import {TooltipTddComponent} from '../';
import * as moment from 'moment';
import * as _ from 'lodash';

/**
 * Componente que muestra la pantalla de movimientos 
 * posibles a aclarar
 * Ademas de los filtros y el repositorio de movimientos
 *
 * @export
 * @class WelcomeComponentTDD
 * @implements {OnInit}
 * @implements {OnDestroy}
 */
@Component({
    selector: 'app-welcome-tdd',
    templateUrl: './welcome-tdd.component.html',
    providers: [
      DataService
    ]
})
    export class WelcomeTddComponent implements OnInit {

   @ViewChild(forwardRef(() =>TooltipTddComponent)) tooltip:TooltipTddComponent;
      
  //SETEO VARIABLES

  // Token
  token : string;

  //Spinner
  spinner: boolean;

  //Filtros

  sfApply : boolean = false;
  filterApply : boolean = false;
  filterName : string = '';
  enable : boolean = false;
  actualFilter : number;

  //Channel
  chanelType : string = '';

  //MOvimientos globales
  globalMoves = [];
  allMoves = [];
  viewMoves =[];
  filterMoves = [];
  filterNameApply = '';

  //Selected Moves
  smSelectedMoves = [];
  viewSelectedMoves =[];

  //Bandera carga movimientos
  isLoadingMoves:boolean;

/**
 *Creates an instance of WelcomeTddComponent.
 * @param {ActivatedRoute} route
 * @param {Router} router
 * @param {Location} location
 * @param {SessionStorageService} storage
 * @param {HttpClient} http
 * @param {DataService} dataService
 * @param {UtilsTddService} utils
 * @memberof WelcomeTddComponent
 */
constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private storage: SessionStorageService,
    private http: HttpClient,
    private dataService: DataService,
    private utils: UtilsTddService
  ) {

    //DEC VARIBALES

    //Spinner
    this.spinner = true;

    //GET URL PARAMETERS
    route.paramMap.subscribe(params => {
      this.token = params.get('token');
    });

    //Get Channel
    this.chanelType = this.storage.getFromLocal('chanel');
    //this.chanelType = 'wallet';


  }


  /**
   * Metodo que carga las urls al servicio dataService
   * hace el llamado a los movimientos del usuario
   *
   * @memberof WelcomeTddComponent
   */
  ngOnInit() {
    this.saveSession();
    this.dataService.setUris(this.storage.getFromLocal('enviroment'));
    this.isLoadingMoves = true;
    this.callMoveService(0,0);
  }


  /**
   * Hace el cambio de los movimientos por mes a un array global
   * los movimientos de la vista son generados de la lista global
   * Generar movimientos 
   *
   * @memberof WelcomeTddComponent
   */
  generateMovesArray(){
    this.allMoves = [];
    let extract =0;
    for (let i of this.globalMoves){
      for (let j of i){
        this.allMoves.push(this.utils.generateMove(j,extract));
      }
      extract +=1;
    }
    this.viewMoves = this.generateViewMoves(this.allMoves);
  }

  
  /**
   * Filtrar Moviemientos
   * Genera un array de el mes seleccionado
   * recibe el indice para tomarlo del arreglo de meses
   * genera la vista del filtro
   *
   * @param {*} moves
   * @param {number} index
   * @memberof WelcomeTddComponent
   */
  generateFilterMoves(moves:any,index:number){
    this.filterMoves=[];
    for (let i of moves){
      this.filterMoves.push(this.utils.generateMove(i,index));
    }
    this.viewMoves = this.generateViewMoves(this.filterMoves);


    //console.log(index);
  }


  deleteFilters(){
    

  }

  

  /**
   * Generar Movimientos Vista
   * genera un mapa con fecha,array
   * la fecha se usa como llave para la vista
   * el array contiene los movimientos de esa fecha
   * 
   * 
   * @param {*} moves
   * @returns {*}
   * @memberof WelcomeTddComponent
   */
  generateViewMoves(moves:any):any{
    let tempArray = [];
    if(moves.length>0){
      for(let move of moves){
        let index = _.findIndex(tempArray,(o)=>{
          return o.key.getTime() === move.typeDate.getTime();
        })
        if(index >= 0){
          tempArray[index].value.push(move);
        } else {
          tempArray.push({key:move.typeDate,value:[move]});
        }
      }
    }
    return tempArray;

  }

  
  /**
   *LLAMADO AL SERVICIO D EMOVIMIENTOS
   *
   * hace el llamado de movimientos
   * se hace el llamado 4 veces o al segundo reintenro
   * una vez terminado el llamado genera el array global y de la vista
   * 
   * 
   * @private
   * @param {number} month
   * @param {number} retry
   * @memberof WelcomeTddComponent
   */
  private callMoveService(month:number,retry:number){
    let endpoint = this.getMovesEndpoint(month);
    const httpOptions = this.utils.getHeadersRequest();
    if(this.globalMoves.length < 4 ){
      this.http.get(endpoint,httpOptions).subscribe(
        data => {
          this.globalMoves.push(data);
          this.callMoveService(month+1,0);
        },
        err => {
          if(retry < 1){
            this.callMoveService(month,retry+1);
          }else{
            this.isLoadingMoves = false;
            this.generateMovesArray();
          }
        }
      );  
    }else{
      this.isLoadingMoves = false;
      this.generateMovesArray();
    }
  }

  /**
   *Selected Moves View
   *
   * genera el array que se muestra en el repositorio de movimientos
   * mapa fecha - array
   * @memberof WelcomeTddComponent
   */
  generateSelectedView(){
    this.viewSelectedMoves=[];
    this.viewSelectedMoves = this.generateViewMoves(this.smSelectedMoves);
    this.viewSelectedMoves = _.orderBy(this.viewSelectedMoves,'key','desc')
  }

  /**
   * Movimientos Seleccionados
   * agrega los movimientos que se vayan seleccionando en la vista
   * a un mapa fecha - array
   * si la fecha del movimiento existe en el mapa lo agrega a su array
   * si no agrega la llave y al array con el movimiento
   *
   * @param {*} move
   * @returns {*}
   * @memberof WelcomeTddComponent
   */
  selectedMoves (move: any): any {

    const index = _.findIndex(this.smSelectedMoves,(o)=>{
      return o.id === move.id;
    })

    if(index >= 0){
      this.smSelectedMoves.splice(index,1);
    }else{
      this.smSelectedMoves.push(move);
    }
    this.checkSelectedElements(move)
  }


  // OBTENER FECHA
  /**
   * regresa el año que se debe consultar
   * recibe el numero de mes que se va a restar a la fecha actual
   *
   * @param {number} month
   * @returns
   * @memberof WelcomeTddComponent
   */
  getMovesYear(month:number){
    return Number( moment().subtract(month,'months').format('YYYY'));
  }

  /**
   * regresa el mes que se debe mostrar en la vista - filtro
   * recibe el numero de mes que se va a restar a la fecha actual
   *
   * @param {number} month
   * @returns
   * @memberof WelcomeTddComponent
   */
  getMonthName(index:number){
    if(index === 0){
      return "Corte actual"
    }else{
      let tempstr = moment().subtract(index,'months').format('MMMM');
      return tempstr.charAt(0).toUpperCase() + tempstr.slice(1);
    }
  }

  /**
   * regresa el mes que se debe consultar
   * recibe el numero de mes que se va a restar a la fecha actual
   *
   * @param {number} month
   * @returns
   * @memberof WelcomeTddComponent
   */
  getMovesMonth(month:number){
    return Number(moment().subtract(month,'months').format('MM'));
  }

  /**
   * genera el endpoint que se debe consultar
   * dependiendo de el ambiente y del modo dummy
   *
   * @param {number} month
   * @returns {string}
   * @memberof WelcomeTddComponent
   */
  getMovesEndpoint(month:number):string{
    let endpoint = '';
    if (this.storage.getFromLocal('dummy')) {
      endpoint = 'assets/data/moves-tdd.'+month+'.json'; 
    } else{ 
      endpoint = this.dataService.getApiURL('debit')+'/movements/year/'+this.getMovesYear(month)+'/month/'+this.getMovesMonth(month);
    }
    return endpoint;
  }

  //GET CLIENT ID
  /**
   * una vez cargado el elemento de la vista 
   * esconde el spinner
   *
   * @private
   * @memberof WelcomeTddComponent
   */
  private saveSession(): void {
    // session
      this.spinner=false;

  }
  
  /**
   * Check if movement is selected and return true
   * to add "active" class.
   *
   * @param {*} element
   * @returns {boolean}
   * @memberof WelcomeTddComponent
   */
  checkSelectedElements(element : any) : boolean{
    let value = false;
    const index = _.findIndex(this.smSelectedMoves,(o)=>{
      return o.id === element;
    })
    if(index >= 0) {
      value = true;
    }
    return value;
  }


  /**
   * muestra un lightbox cuando se da click
   * filtro o repositorio de movimientos
   *
   * @param {*} id
   * @memberof WelcomeTddComponent
   */
  showToggle(id){
    let element = document.getElementById(id);
    element.classList.add("animate");
  }

  /**
   * esconde un lightbox cuando se da click
   * filtro o repositorio de movimientos
   *
   * @param {*} id
   * @memberof WelcomeTddComponent
   */
  hideToggle(id){
    let element = document.getElementById(id);
    element.classList.remove("animate");
    element.classList.add("animateReverse");
    setTimeout(function(){
      element.classList.remove("animateReverse");
    }, 700);

    if(id === 'filtersComp'){
      this.filterApply = true;
    }
  }


  /**
   * muestra el lightbox del child tooltip
   * genera el mensaje del tooltip y lo pasa como parametro al child
   *
   * @param {*} evt
   * @param {*} id
   * @memberof WelcomeTddComponent
   */
  public showTooltip(evt:any, id){
    let text ='';
    if(id === 1){
      text = 'Por favor toque “x” para eliminar un movimiento que ya no desee mandar a aclarar.';
    }else{
      text = 'Es importante señalar que se pueden aclarar movimientos de hasta 3 meses anteriores a la fecha actual.';
    }
    this.tooltip.showTooltip(evt,text);
  }

  /**
   * metodo que recibe del hijo la accion de continuar
   * guarda los movimientos seleccionados en sesion y navega a el cuestionario
   *
   * @memberof WelcomeTddComponent
   */
  executeContinue(){
    this.storage.saveInLocal('multifolio',this.smSelectedMoves);
    this.router.navigate(['questionnaireTDD']);
  }



  /**
   * muestra la aplicación de los filtros en la vista
   *
   * @memberof WelcomeTddComponent
   */
  showFilterApply(){
    if(this.sfApply){
      this.sfApply = false;
    }else{
      this.sfApply = true;
    }
  }

}