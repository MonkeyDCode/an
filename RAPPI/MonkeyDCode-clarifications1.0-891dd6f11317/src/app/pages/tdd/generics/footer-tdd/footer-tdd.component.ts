import { Component, OnInit,EventEmitter, Input, Output } from '@angular/core';
import {SessionStorageService} from '../../../../services/tdd/session-storage.service';
/**
 * footes de la aplicacion
 * boton de continuar de la app
 *
 * @export
 * @class FooterTddComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-footer-tdd',
  templateUrl: './footer-tdd.component.html'
})
export class FooterTddComponent implements OnInit {
  /**activo a desactivo el boton */
  @Input() enable: boolean;
  /**texto a mostrar en el boton */
  @Input() buttonText: string;
  /**emisor al padre para continuar */
  @Output() continueClarification = new EventEmitter<any>();
/**
 *
 *
 * @memberof FooterTddComponent
 */


  /**Channel*/
  chanelType : string = '';
/**
 *Creates an instance of FooterTddComponent.
 * @param {SessionStorageService} storage
 * @memberof FooterTddComponent
 */
constructor( private storage: SessionStorageService, ) {
    //Get Channel
    this.chanelType = this.storage.getFromLocal('chanel');
    //this.chanelType = 'wallet';
  }
/**
 *inicializa el componente
 *
 * @memberof FooterTddComponent
 */
ngOnInit() {
  }
/**
 *Method when is clicked the footer
 *
 * @private
 * @memberof FooterTddComponent
 */
private continue(){ 
    this.continueClarification.emit();
  }

}
