import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ISubscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { DataService } from '../../../services/data.service';
import { MoveModel, UserModel, ExtractModel, LoaderModel } from '../../../models';
// Constants
import { ConstantsService } from '../../../services/constants.service';

import { AlertComponent } from '../../../partials';

//TDD
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {SessionStorageService} from '../../../services/tdd/session-storage.service';



import * as _ from 'lodash';
import * as moment from 'moment';
/**
 *
 *
 * @export
 * @class PreloaderTddComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-preloader-tdd',
  templateUrl: './preloader-tdd.component.html',
  providers: [
    DataService  ]
})
export class PreloaderTddComponent implements OnInit {
  /**entorno de la aplicacion */
  private enviroment: any;
  /**subscripcion de las respuestas de el http */
  private subscription: ISubscription;
  /**datos del usuario */
  private userData: UserModel;
  /**movimientos del usuario almacenados en un array */
  private dataHandler = Array<MoveModel>();
  /**movimientos seleccionados */
  private selectionHandler = Array<MoveModel>();
  /**datos recibidos de los servicios */
  private rawData: any;
  /**datos de la tarjeta de debito */
  private ccData: any;
  /**descripcion del filtro */
  private filterDesc = '';
  /**filtro aplicado a los movimientos */
  private currentFilter: string;
  /**cantidad de fechas */
  private dateArraysQuantity = 0;
  /**variable del modo dummy */
  private dummyMode = false;
  /**variable de la carga de un servicio */
  private isLoading = false;
  /**numero de tarjeta de entrada */
  private cardNumber = '';
  /**loader que muestra la app */
  private loader: LoaderModel;
  /**token de la aplicacion */
  private token = '';
  /**servicio de modals */
  private modalRef: BsModalRef;

  /**
   * Creates an instance of PreloaderTddComponent.
   * @param {DataService} dataService
   * @param {ActivatedRoute} route
   * @param {Router} router
   * @param {ConstantsService} constantsService
   * @param {BsModalService} modalService
   * @memberof PreloaderTddComponent
   */
  constructor(
    private dataService: DataService,
    private route: ActivatedRoute,
    private router: Router,
    private constantsService: ConstantsService,
    private modalService: BsModalService,
    private storage: SessionStorageService,
    public http: HttpClient

  ) { }
  /**
   * Angular Lifecycle hook: When the component it is initialized.
   *
   * @memberof PreloaderTddComponent
   */
  public ngOnInit(): void {
    // Get and Set the enviroment
    this.subscription = this.dataService
      .restRequest(
        'config.json',
        'GET',
        {},
        'config'
      )
      .subscribe(
        (response) => this.parseConfiguration(response),
        (error) => this.storage.saveInLocal('enviroment','dev')
      );
  }
  /**
   * Parse the alias of the configuration.
   *
   * @private
   * @param {*} config
   * @memberof PreloaderTddComponent
   */
  private parseConfiguration(config: any): void {
    // Search the enviroment
    this.enviroment = _.find(this.constantsService.ENVIROMENT, (item) => {
      return item.env === config.ENV_VAR;
    });
    if (!_.isUndefined(this.enviroment)) {
      this.storage.saveInLocal('enviroment',this.enviroment.env)
      this.dataService.setUris(this.enviroment.env);
      this.getQueryParams();
    } else {
      this.handleError('No enviroment');
    }
  }
  /**
   * Get the params in the URL.
   *
   * @private
   * @memberof PreloaderTddComponent
   */
  private getQueryParams(): void {
    let paramDummy: any = '';
    let paramNoLock: any = '';
    let chanel: any = '';
    this.route.queryParams.subscribe((params) => {
      this.token = params['token'];
      paramDummy = params['dummy'];
      paramNoLock = params['nolock'];
      chanel = params['chanel']; 
      if(chanel === undefined){
        chanel = 'default';
      }
      this.storage.saveInLocal('chanel',chanel);
      this.storage.saveInLocal('nolock',false);
      this.storage.saveInLocal('dummy',false);
      if (paramNoLock === 'true') {
        this.storage.saveInLocal('nolock',true)
      }
      if (paramDummy === 'true') {
        this.dummyMode = paramDummy;
        this.storage.saveInLocal('pan',1234567890123456);
        this.storage.saveInLocal('dummy',true);
      }

      this.postOAuthToken();
    });
  }
  /**
   * Post OAuth Token.
   *
   * @private
   * @memberof PreloaderTddComponent
   */
  private postOAuthToken(): void {
    if (this.dummyMode) {
      this.subscription = this.dataService.dummyRequest('assets/data/token.json')
        .subscribe((response) => this.processTokenOAuth(response)); // DUMMY MODE
    } else {
      this.dataService
        .restRequest(
          'token', 'POST', {}, 'token'
        )
        .subscribe(
          (response) => { this.processTokenOAuth(response); },
          (error) => this.handleError(error)
        );
    }
  }
  /**
   * Process token OAuth.
   *
   * @private
   * @param {*} r
   * @memberof PreloaderTddComponent
   */
  private processTokenOAuth(r: any): void {
    this.storage.saveInLocal('app-access',r.access_token);
    if (this.dummyMode) {
      this.aquireClientData();
    } else {
      this.getID();
    }
  }
  /**
   * Get the ID from the token validator.
   *
   * @private
   * @memberof PreloaderTddComponent
   */
  private getID(): void {
    const params = {
      token: this.token
    };
    this.dataService
      .restRequest('/token-validator', 'POST', params, 'sso_token', this.storage.getFromLocal('app-access'))
      .subscribe(
        (response) => {
          if (response.id && response.pan) {
            this.cardNumber = response.pan;
            this.storage.saveInLocal('client', response.id);
            this.storage.saveInLocal('pan', response.pan.toString().slice(-4));
            this.storage.saveInLocal('buc', response.buc);
            this.storage.saveInLocal('category', response.menu);
            this.storage.saveInLocal('segment', response.segmento);
            const channel = response.canal.toLowerCase();
            if (channel === 'wallet') {
              this.storage.saveInLocal('chanel', channel);
            } else {
              this.storage.saveInLocal('chanel', 'default');
            }
            this.aquireClientData();
          }
        },
        (error) => this.handleError(error)
      );
  }
  /**
   * Aquire client data.
   *
   * @private
   * @param {string} cardID
   * @returns {void}
   * @memberof PreloaderTddComponent
   */
  private aquireClientData():void {
    //this.loader.setMessage('Obteniendo información de usuario');
    if (this.dummyMode) {
      this.subscription = this.dataService.dummyRequest('assets/data/ccdata.json')
        .subscribe((response) => this.processCCData(response)); // DUMMY MODE
    } else {
      this.subscription = this.dataService
        .restRequest(
          '/ccdata/',
          'POST',
          {},
          '',
          this.storage.getFromLocal('app-access'),
          this.storage.getFromLocal('client')
        )
        .subscribe(
          (response) => this.processCCData(response),
          (error) => this.handleError(error)
        );
    }
  }
  /**
   * Process CCData.
   *
   * @private
   * @param {*} r
   * @memberof PreloaderTddComponent
   */
  private processCCData(r: any): void {
    if (!_.isUndefined(r.codigoMensaje)) {
      this.showAlert();
    }
    this.storage.saveInLocal('ccdata',r);
    this.ccData = r;
    this.getStatesCatalog();
  }
  /**
   * Get the states from a JSON file.
   *
   * @private
   * @memberof PreloaderTddComponent
   */
  private getDummyStates(): void {
    this.subscription = this.dataService.dummyRequest('assets/data/states.json')
          .subscribe((response) => this.processStates(response));
  }
  /**
   * Get states catalog, if the size if the response is zero, get from the dummy file.
   *
   * @private
   * @memberof PreloaderTddComponent
   */
  private getStatesCatalog(): void {
    //this.loader.setMessage('Obteniendo Entidades Federativas');
    if (this.dummyMode) {
      this.getDummyStates();
    } else {
      this.subscription = this.dataService.restRequest(
        '/catalog/states',
        'GET',
        '',
        'catalogs',
        this.storage.getFromLocal('app-access'),
        this.storage.getFromLocal('client')
      ).subscribe(
        (response) => {
          if (_.size(response) > 0) {
            this.processStates(response);
          } else {
            this.getDummyStates();
          }
        },
        (error) => {
          this.getDummyStates();
        }
      );
    }
  }
  /**
   * Process States.
   *
   * @private
   * @param {*} v
   * @memberof PreloaderTddComponent
   */
  private processStates(v: any): void {
    let idxstate= _.findIndex(v, ['clave' , 9]);
    if (idxstate !== -1) v[idxstate].nombre = 'Ciudad de México';
    idxstate= _.findIndex(v, ['clave' , 12]);
    if (idxstate !== -1)  v[idxstate].nombre = 'Guerrero';
    idxstate= _.findIndex(v, ['clave' , 15]);
    if (idxstate !== -1)  v[idxstate].nombre = 'Estado de México';
    this.storage.saveInLocal('states',v);
    this.processUserData();
    //this.aquireExtracts();
    // TODO: Get the Categories and SubCategories
  }

  /**
   * Process user data and save them in the local storage.
   *
   * @private
   * @param {Response} response
   * @memberof PreloaderTddComponent
   */
  private processUserData(): void {
    this.subscription.unsubscribe();
    let usrdata = {
      buc: this.storage.getFromLocal('buc'),
      name: this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo.cardEmbossName,
      cardNumber: this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo.cardNum,
      cardName: this.ccData.cardAcctRelRec[0].cardAcctRelInfo.acctRef.acctRec.acctInfo.desc,
      cardBrand : this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo.brand,
      saldo: this.getBalance('AVAIL')
    }
    this.storage.saveInLocal('userdata',usrdata);
    this.preloaderComplete();
  }

/**
 *get the balance dependig the type
 *
 * @private
 * @param {string} type
 * @returns {number}
 * @memberof PreloaderTddComponent
 */
private getBalance(type: string): number {
    let res = 0;
    const full: any = this.ccData;
    const v: any = full.cardAcctRelRec[0].cardAcctRelInfo.acctRef.acctRec.acctInfo.acctBalances;
    _.forEach(v, function(item: any){
      if(item.balType.balTypeValues === type){
        res = parseFloat(item.curAmt.amt);
      }
    });
    return res;
  }

  /**
   * Show the error alert.
   *
   * @private
   * @memberof PreloaderTddComponent
   */
  private showAlert(): void {
    const options: any = {
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.modalRef = this.modalService.show(AlertComponent, options);
    this.modalRef.content.type = 'servicesError';
  }
  /**
   * Handle error
   *
   * @private
   * @param {*} error
   * @returns {*}
   * @memberof PreloaderTddComponent
   */
  private handleError(error: any): any {
    return error;
  }
  /**
   * Preloader complete.
   *
   * @private
   * @memberof PreloaderTddComponent
   */
  private preloaderComplete(): void {
    this.router.navigate(['welcomeTDD']);
  }
}
