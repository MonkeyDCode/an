import { Component, OnInit,ViewChild,forwardRef, Input} from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import * as moment from 'moment';
import {TooltipTddComponent} from '../';
import {UtilsTddService} from '../../../services/tdd/utils-tdd.service';
import {SessionStorageService} from '../../../services/tdd/session-storage.service';
import * as _ from 'lodash';
import { AlertsMain } from '../alerts-tdd/alertsMain';


/**
 *
 *
 * @export
 * @class QuestionnaireTddComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-questionnaire-tdd',
  templateUrl: './questionnaire-tdd.component.html'
})
export class QuestionnaireTddComponent implements OnInit {
  /** tooltip agregado como child */
  @ViewChild(forwardRef(() =>TooltipTddComponent)) tooltip:TooltipTddComponent;
  /**tiene la tarjeta en su poder */
  private hasCard;
  /**interactuo con el comercio */
  private commerceInteraction;
  /**motivo seleccionado  */
  private motiveSelected;
  /**id del motivo seleccionado */
  private motiveSelectedId;
  /**descripcion de lo sucedidp */
  private motiveClientDescription;
  /**validacion de la longitud de la descripcion */
  private validMotiveClientDescription;
  /**fecha en l que perdio la tarjeta */
  private lostDate;
  /**dia en que perdio la tarjeta */
  private lostDateDay;
  /**mes en que perdio la tarjeta */
  private lostDateMonth;
  /**año en que reporto la tarjeta */
  private lostDateYear;
  /**ubicacion del cliente */
  private location;
  /**estado donde se encuentra el cliente */
  private state;
  //**días del mes */
  private days;
  /**meses del año */
  private months;
  /**años a seleccionar */
  private years;
  /**catalogo de estados */
  private states = [{ clave:0 , nombre:''}];

  /**motivos a mostrar en la vista */
  //8-La reporte como robada o extraviada
  //9-Nunca la tuve conmigo
  //10 = Me la robaron o la extravié y no la he reportado
  private motives=[
    {id:'1',description:'Cargo duplicado',label:'Describa lo que <b>sucedió</b>:',hover:'Ejem.: No reconozco el cargo porque me aparece en más de una ocasión.',tooltip:'Se presenta cuando en los movimientos aparecen 2 importes por la misma compra.'},
    {id:'2',description:'Monto alterado',label:'¿Cuál es el importe que <strong>autorizó y reconoce</strong>?',hover:'Ejem.: Es de $120,000.00 el monto que si reconozco.',tooltip:'Se presenta cuando se realizó  una compra y el movimiento tiene una cantidad diferente a la que se reconoce.'},
    {id:'3',description:'Cargos adicionales al autorizado',label:'Indique los datos del (los) <strong>consumo(s) que sí reconoce</strong>:',hover:'Ejem.: ID 2429384 - Compra en Liverpool',tooltip:'Se presenta cuando aparecen distintos o mayor número de importes.'},
    {id:'4',description:'Pago por otro medio',label:'Describa lo que <strong>sucedió</strong>:',hover:'Ejem.: No hice el pago a través de este medio.',tooltip:'Se presenta cuando el pago con la tarjeta falla, y aun así se realiza el cargo, aunque se haya pagado con otro medio (efectivo u otra tarjeta).'},
    {id:'5',description:'Devolución no aplicada',label:'<strong>Comentarios</strong> adicionales:',hover:'Ejem.: No me han hecho mi devolución.',tooltip:'Se presenta cuando se realizó una compra, se devolvió el objeto y en el estado de cuenta el cargo fue aplicado.'},
    {id:'6',description:'Mercancías o servicios no proporcionados',label:'<strong>Comentarios</strong> adicionales:',hover:'Ejem.: Nunca me llegó mi mercancía',tooltip:'Se presenta cuando se hizo una compra, nunca llegó el objeto y aun así el cargo fue aplicado.'},
    {id:'7',description:'Cancelación de servicio',label:'<strong>Comentarios</strong> adicionales:',hover:'Ejem.: No estoy de acuerdo con el cobro del servicio, por lo cual lo cancelo.',tooltip:'Se presenta cuando se estableció un pago de servicio, se canceló el servicio y aun así se sigue aplicando el cargo.'},
  ]

  /**
   *Creates an instance of QuestionnaireTddComponent.
  * @param {UtilsTddService} utils
  * @param {SessionStorageService} storage
  * @param {Router} router
  * @memberof QuestionnaireTddComponent
  */
  constructor( 
    private utils:UtilsTddService,
    private storage: SessionStorageService,
    private router: Router,
    private alertsMain : AlertsMain
  
  ) {
    this.hasCard='';
    this.resetQuestionary(1);
    this.generateDates();
    this.states = this.storage.getFromLocal('states') ;
    this.states.splice(-1,1);
   }

  /**
   *
   *
   * @memberof QuestionnaireTddComponent
   */
  ngOnInit() {

    //this.alertsMain.sendMessage(-1,false);
  }
  
  /**
  * Method to go botoom page
  *
   *
   *
   * @private
   * @memberof QuestionnaireTddComponent
   */
  private scrollBottom(){
    setTimeout(()=>{
      window.scrollTo(0,document.body.scrollHeight);
    },150)
    
  }

  /**
  * Method that generates the values of the date
   *
   * @memberof QuestionnaireTddComponent
   */
  generateDates(){
    this.days =['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
    this.months=['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC'];
    let date = moment(); 
    this.years=[date.format('YYYY')];
    for(let i=0;i<10;i++){
      this.years.push(date.subtract(1,'years').format('YYYY'));
    }  
  }


  /**
  * Input validator for the comments
  *
   *
   * @param {*} event
   * @memberof QuestionnaireTddComponent
   */
  inputValidator(event: any) {
    const pattern = /^[a-zA-Z0-9 _\\-\\.:,;áéíóúÁÉÍÓÚÜü¿?"¡!#$%&()=]*$/;   
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9 _\\-\\.:,;áéíóúÁÉÍÓÚÜü¿?"¡!#$%&()=]/g, "");
    }
  }
  /**
   *Validate the description lenght
   *
   * @memberof QuestionnaireTddComponent
   */
  validateMotiveDescription(){
      this.motiveClientDescription.length >=1 ? this.validMotiveClientDescription = true: this.validMotiveClientDescription = false;
  }

  /**
  * set the correct value to motiveSelected when changes the motive
   *
   * @param {*} id
   * @memberof QuestionnaireTddComponent
   */
  changeMotive(id){
    if(Number(id) === 8 || Number(id) === 9 ||Number(id) === 10 ){
      this.motiveSelected = {id:id };
    }else{
      this.motiveSelected = this.motives[Number(id)-1];
    }
  }

  /**
  * reset the value of the questionary depending the level of reset
  *
   *
   * @param {*} level
   * @memberof QuestionnaireTddComponent
   */
  resetQuestionary(level){
    switch(level){
      case 1:
        this.commerceInteraction='';
        this.motiveSelectedId='0';
        this.motiveSelected={id:'0'}
        this.motiveClientDescription='';
        this.validMotiveClientDescription=false;
        this.lostDate='';
        this.lostDateDay='';
        this.lostDateMonth='';
        this.lostDateYear='';
        this.state='';
        this.location='';
        break;
      case 2:
        this.motiveSelectedId='0';
        this.motiveSelected={id:'0'}
        this.motiveClientDescription='';
        this.validMotiveClientDescription=false;
        this.lostDate='';
        this.lostDateDay='';
        this.lostDateMonth='';
        this.lostDateYear='';
        this.state='';
        this.location='';
        break;
      case 3:
        this.motiveClientDescription='';
        this.validMotiveClientDescription=false;
        this.lostDate=''; 
        this.lostDateDay='';
        this.lostDateMonth='';
        this.lostDateYear='';
        this.state='';
        this.location='';
        break;
      case 4:
        this.location='';
        this.state='';
        break;
      case 5:
        this.state='';
        break;
      case 6:
        this.validMotiveClientDescription=false;
        break;
      default:
        break;
    }
  }

  /**
   * Check if the lost date is valid
   *
   *
   *
   * @memberof QuestionnaireTddComponent
   */
  public validateDate() {
    if(this.lostDateDay!=='' && this.lostDateMonth!=='' && this.lostDateYear!==''){
      if(moment(`${this.lostDateDay}/${this.lostDateMonth}/${this.lostDateYear}`,'DD/MMM/YYYY','es').isValid()){
        this.lostDate = moment(`${this.lostDateDay}/${this.lostDateMonth}/${this.lostDateYear}`,'DD/MMM/YYYY','es').format('DD-MMM-YYYY');
      }
    }
  }

  /**
   * generates the multifolio object 
   *
   *
   * @memberof QuestionnaireTddComponent
   */
  validateForm(){
    let questionnaire = [];
    let viewQuestions = [];
    let viewLocation = '';
    if(this.hasCard==='SI'){
      if(Number(this.motiveSelectedId) !== 5){
        questionnaire = questionnaire.concat(this.generateNonrecognized());
      }
    }else if(this.hasCard==='NO'){
      let question ={ Preguntas:'¿Reporto usted su tarjeta de crédito o débito como robada o extraviada a Banco Santander?',
                      Respuestas:''};
      if(Number(this.motiveSelectedId) !== 8 ){
        question.Respuestas = 'NO';
        questionnaire.push(question);
      }else if(Number(this.motiveSelectedId) !== 10){
        question.Respuestas = 'SI';
        questionnaire.push(question);
      }

      

    }
    if(this.state !== ''){
      viewLocation = _.find(this.states,(item)=>{
        if(Number(item.clave) === this.state){
          return item.nombre.toUpperCase();
        }
      }).nombre;
    }else{
      viewLocation = 'FUERA DE LA REPUBLICA'
    }
    viewQuestions = this.utils.generateViewQuestions(this.motiveSelected,this.motiveClientDescription,this.lostDate,viewLocation);
    this.storage.saveInLocal('viewQuestions',viewQuestions);
    this.storage.saveInLocal('questionnaire',questionnaire);
    let actualLocation;
    this.location==='Foreign'?actualLocation = 33:actualLocation = this.state;
    let adittionaldata={
      description:[this.motiveClientDescription],
      lostdate:this.lostDate,
      location: actualLocation
    }
    this.saveSubcategory();
    this.storage.saveInLocal('additionaldata',adittionaldata);
    this.storage.saveInLocal('questionId',this.motiveSelected);
    this.router.navigate(['summaryTDD']);
  }

  /**
   *validates the subcategory and saves in the storage
   *
   * @memberof QuestionnaireTddComponent
   */
  saveSubcategory(){
    switch(Number(this.motiveSelectedId)){
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 6:
      case 7:
        this.storage.saveInLocal('subcategory','COMPRA NO RECONOCIDA');
        break;
      case 5:
        this.storage.saveInLocal('subcategory','DEVOLUCION NO APLICADA');
        break;
      case 8:
      case 10:
        this.storage.saveInLocal('subcategory','TARJETA ROBADA O EXTRAVIADA');
        break;
      case 9:
      this.storage.saveInLocal('subcategory','TARJETA NO RECIBIDA');
        break;
      default:
        this.storage.saveInLocal('subcategory','COMPRA NO RECONOCIDA');
        break;
    }
  }


  /**
   *generates the questions when he fow is not recognized
   *
   * @returns
   * @memberof QuestionnaireTddComponent
   */
  generateNonrecognized(){
    let questions = [
      {Preguntas:'¿Tiene la tarjeta en su poder?',Respuestas:'SI'}
    ];
    let tempQuestion = { Preguntas:'¿Interactuó con el comercio durante la compra?', Respuestas:''};
    this.commerceInteraction==='SI' ? tempQuestion.Respuestas = 'SI': tempQuestion.Respuestas = 'NO';
    questions.push(tempQuestion);
    questions = questions.concat(this.getAdditionalQuestionary());
    return questions;
  }


  /**
   *Additional questions in non recognized
   *
   * @returns
   * @memberof QuestionnaireTddComponent
   */
  getAdditionalQuestionary(){
    let questionary = [
      {Preguntas:'Cargo duplicado',Respuestas:'NO'},
      {Preguntas:'Monto alterado',Respuestas:'NO'},
      {Preguntas:'Cargos adicionales',Respuestas:'NO'},
      {Preguntas:'Servicios no proporcionados',Respuestas:'NO'},
      {Preguntas:'Mercancia defectuosa',Respuestas:'NO'},
      {Preguntas:'Pago por otro medio',Respuestas:'NO'},
      {Preguntas:'Cancelación de servicio',Respuestas:'NO'},
      {Preguntas:'Otro',Respuestas:'NO'}
    ];

    switch(Number(this.motiveSelectedId)) { 
      case 1: { 
        questionary[0].Respuestas='SI'; 
        break; 
      } 
      case 2: { 
        questionary[1].Respuestas='SI'; 
        break; 
      } 
      case 3: { 
        questionary[2].Respuestas='SI';  
        break; 
      } 
      case 4: { 
        questionary[3].Respuestas='SI'; 
        break; 
      }  
      case 6: { 
        questionary[5].Respuestas='SI'; 
        break; 
      } 
      case 7: { 
        questionary[6].Respuestas='SI'; 
        break; 
      }
      default: { 
        break; 
      }
    }

    return questionary;
  }


  /**
   *method tho execute the child component
   *
   * @param {*} evt
   * @memberof QuestionnaireTddComponent
   */
  public showTooltip(evt:any){
    this.tooltip.showTooltip(evt,this.motiveSelected.tooltip);
  }


  /**
   * muestra el alert cuando se seleccione no en el cuestionario
   * 
   *
   * @memberof QuestionnaireTddComponent
   */
  showAlert(){
    this.alertsMain.sendMessage(4,true);
  }
}
