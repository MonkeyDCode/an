import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ISubscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { DataService } from './../../services/data.service';
import { MoveModel, UserModel, ExtractModel, LoaderModel } from './../../models';
import { DataProxyService } from './../../services/data-proxy.service';
// Constants
import { ConstantsService } from './../../services/constants.service';

import { AlertComponent } from './../../partials/';

import * as _ from 'lodash';
import * as moment from 'moment';
/**
 *
 *
 * @export
 * @class PreloaderComponent
 * @implements {OnInit}
 */
@Component({
  selector: 'app-preloader',
  templateUrl: './preloader.component.html',
  providers: [
    DataService,
    DataProxyService
  ]
})
export class PreloaderComponent implements OnInit {
  private enviroment: any;
  private subscription: ISubscription;
  private userData: UserModel;
  private dataHandler = Array<MoveModel>();
  private selectionHandler = Array<MoveModel>();
  private rawData: any;
  private ccData: any;
  private filterDesc = '';
  private currentFilter: string;
  private dateArraysQuantity = 0;
  private dummyMode = false;
  private isLoading = false;
  private cardNumber = '';
  private loader: LoaderModel;
  private token = '';
  private modalRef: BsModalRef;

  /**
   * Creates an instance of PreloaderComponent.
   * @param {DataService} dataService
   * @param {DataProxyService} dataProxyService
   * @param {ActivatedRoute} route
   * @param {Router} router
   * @param {ConstantsService} constantsService
   * @param {BsModalService} modalService
   * @memberof PreloaderComponent
   */
  constructor(
    private dataService: DataService,
    public dataProxyService: DataProxyService,
    private route: ActivatedRoute,
    private router: Router,
    private constantsService: ConstantsService,
    private modalService: BsModalService
  ) { }
  /**
   * Angular Lifecycle hook: When the component it is initialized.
   *
   * @memberof PreloaderComponent
   */
  public ngOnInit(): void {
    this.loader = new LoaderModel();
    this.dataProxyService.clearData();
    // Get and Set the enviroment
    this.subscription = this.dataService
      .restRequest(
        'config.json',
        'GET',
        {},
        'config'
      )
      .subscribe(
        (response) => this.parseConfiguration(response),
        (error) => this.dataProxyService.setEnviroment('dev')
      );
  }
  /**
   * Parse the alias of the configuration.
   *
   * @private
   * @param {*} config
   * @memberof PreloaderComponent
   */
  private parseConfiguration(config: any): void {
    // Search the enviroment
    this.enviroment = _.find(this.constantsService.ENVIROMENT, (item) => {
      return item.env === config.ENV_VAR;
    });
    if (!_.isUndefined(this.enviroment)) {
      this.dataProxyService.setEnviroment(this.enviroment.env);
      this.dataService.setUris(this.enviroment.env);
      this.getQueryParams();
    } else {
      this.handleError('No enviroment');
    }
  }
  /**
   * Get the params in the URL.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private getQueryParams(): void {
    let paramDummy: any = '';
    let paramNoLock: any = '';
    let chanel: any = '';
    this.route.queryParams.subscribe((params) => {
      this.token = params['token'];
      paramDummy = params['dummy'];
      paramNoLock = params['nolock'];
      chanel = params['chanel']; 
      if(chanel === undefined){
        chanel = 'default';
      }
      this.dataProxyService.setChannel(chanel);
      // paramDummy = 'true';
      this.dataProxyService.setNoLock(false);
      this.dataProxyService.setDummyMode(false);
      if (paramNoLock === 'true') {
        this.dataProxyService.setNoLock(true);
      }
      if (paramDummy === 'true') {
        this.dummyMode = paramDummy;
        this.dataProxyService.setPan(params['pan']);
        this.dataProxyService.setOldCard(params['pan']);
        // this.dataProxyService.setPan('5471460091895768');
        this.dataProxyService.setDummyMode(true);
      }
      this.postOAuthToken();
    });
  }
  /**
   * Post OAuth Token.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private postOAuthToken(): void {
    if (this.dummyMode) {
      this.subscription = this.dataService.dummyRequest('assets/data/token.json')
        .subscribe((response) => this.processTokenOAuth(response)); // DUMMY MODE
    } else {
      this.dataService
        .restRequest(
          'token', 'POST', {}, 'token'
        )
        .subscribe(
          (response) => { this.processTokenOAuth(response); },
          (error) => this.handleError(error)
        );
    }
  }
  /**
   * Process token OAuth.
   *
   * @private
   * @param {*} r
   * @memberof PreloaderComponent
   */
  private processTokenOAuth(r: any): void {
    this.dataProxyService.setAccessToken(r.access_token);
    if (!this.ccData) {
      if (this.dummyMode) {
        this.aquireClientData(this.dataProxyService.getPan());
      } else {
        this.getID();
      }
    } else {
      this.dataProxyService.setCreditCardFullData(this.ccData);
      this.ccData = this.dataProxyService.getCCData();
      this.getStatesCatalog();
    }
  }
  /**
   * Get the ID from the token validator.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private getID(): void {
    const params = {
      token: this.token
    };
    this.dataService
      .restRequest('/token-validator', 'POST', params, 'sso_token', this.dataProxyService.getAccessToken())
      .subscribe(
        (response) => {
          if (response.id && response.pan) {
            this.cardNumber = response.pan;
            this.dataProxyService.setIdToken(response.id);
            this.dataProxyService.setPan(response.pan);
            this.dataProxyService.setOldCard(response.pan);
            this.dataProxyService.setBuc(response.buc);
            const channel = response.canal.toLowerCase();
            if (channel.trim() === 'super movil') {
              this.dataProxyService.setChannel('default');
            } else {
              this.dataProxyService.setChannel(channel);
            }
            this.aquireClientData(this.dataProxyService.getPan());
          }
        },
        (error) => this.handleError(error)
      );
  }
  /**
   * Aquire client data.
   *
   * @private
   * @param {string} cardID
   * @returns {(boolean | undefined)}
   * @memberof PreloaderComponent
   */
  private aquireClientData(cardID: string): boolean | undefined {
    if (!cardID) {
      return false;
    }
    this.loader.setMessage('Obteniendo información de usuario');
    if (this.dummyMode) {
      this.subscription = this.dataService.dummyRequest('assets/data/ccdata.json')
        .subscribe((response) => this.processCCData(response)); // DUMMY MODE
    } else {
      this.subscription = this.dataService
        .restRequest(
          '/ccdata/',
          'POST',
          {},
          '',
          this.dataProxyService.getAccessToken()
        )
        .subscribe(
          (response) => this.processCCData(response),
          (error) => this.handleError(error)
        );
    }
  }
  /**
   * Process CCData.
   *
   * @private
   * @param {*} r
   * @memberof PreloaderComponent
   */
  private processCCData(r: any): void {
    if (!_.isUndefined(r.codigoMensaje)) {
      this.showAlert();
    }
    this.dataProxyService.setCCData(r);
    this.ccData = r;
    this.getStatesCatalog();
  }
  /**
   * Get the states from a JSON file.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private getDummyStates(): void {
    this.subscription = this.dataService.dummyRequest('assets/data/states.json')
          .subscribe((response) => this.processStates(response));
  }
  /**
   * Get states catalog, if the size if the response is zero, get from the dummy file.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private getStatesCatalog(): void {
    this.loader.setMessage('Obteniendo Entidades Federativas');
    if (this.dummyMode) {
      this.getDummyStates();
    } else {
      this.subscription = this.dataService.restRequest(
        '/catalog/states',
        'GET',
        '',
        'catalogs',
        this.dataProxyService.getAccessToken()
      ).subscribe(
        (response) => {
          if (_.size(response) > 0) {
            this.processStates(response);
          } else {
            this.getDummyStates();
          }
        },
        (error) => {
          this.getDummyStates();
        }
      );
    }
  }
  /**
   * Process States.
   *
   * @private
   * @param {*} v
   * @memberof PreloaderComponent
   */
  private processStates(v: any): void {
    let idxstate= _.findIndex(v, ['clave' , 9]);
    if (idxstate !== -1) v[idxstate].nombre = 'Ciudad de México';
    idxstate= _.findIndex(v, ['clave' , 12]);
    if (idxstate !== -1)  v[idxstate].nombre = 'Guerrero';
    idxstate= _.findIndex(v, ['clave' , 15]);
    if (idxstate !== -1)  v[idxstate].nombre = 'Estado de México';
    this.dataProxyService.setStates(v);
    this.aquireExtracts();
    // TODO: Get the Categories and SubCategories
  }
  /**
   * Get categories catalog.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private getCategoriesCatalog(): void {
    this.loader.setMessage('Obteniendo catálogos de categorías');
    if (!this.dataProxyService.getCategories()) {
      this.subscription = this.dataService
        .restRequest(
          '/catalog/categories',
          'GET',
          '',
          'catalogs',
          this.dataProxyService.getAccessToken()
        )
        .subscribe((response) => this.processCategories(response));
    } else {
      this.getSubcategories();
    }
  }
  /**
   * Process categories.
   *
   * @private
   * @param {*} v
   * @memberof PreloaderComponent
   */
  private processCategories(v: any): void {
    this.dataProxyService.setCategories(v);
    this.getSubcategories();
  }
  /**
   * Get subcategories.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private getSubcategories(): void {
    this.loader.setMessage('Obteniendo catálogos de subcategorías');
    if (!this.dataProxyService.getSubcategories()) {
      this.subscription = this.dataService.restRequest(
        '/catalog/subcategories',
        'GET',
        '',
        'catalogs',
        this.dataProxyService.getAccessToken()
      )
        .subscribe((response) => this.processSubcategories(response));
    } else {
      this.aquireExtracts();
    }
  }
  /**
   * Process Subcategories.
   *
   * @private
   * @param {*} v
   * @memberof PreloaderComponent
   */
  private processSubcategories(v: any): void {
    this.dataProxyService.setSubcategories(v);
    this.aquireExtracts();
  }
  /**
   * Aquire extracts.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private aquireExtracts(): void {
    if (this.dataProxyService.getDataSelected()) {
      this.dataHandler = this.dataProxyService.getDataSelected();
      this.selectionHandler = this.dataProxyService.getDataSelected();
    }
    this.userData = this.dataProxyService.getUserData();
    if (!this.userData) {
      if (this.dummyMode) {
        this.subscription = this.dataService.dummyRequest('assets/data/extracts.json')
          .subscribe((response) => this.processUserData(response)); // DUMMY MODE
      } else {
        // TODO: Throw an error if the cards has no info
        const cn = this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo.cardNum;
        this.subscription = this.dataService
          .restRequest(
            '/extracts/',
            'POST',
            {},
            'user',
            this.dataProxyService.getAccessToken()
          )
          .subscribe(
            (response) => this.processUserData(response),
            (error) => this.handleError(error)
          );
      }
    } else {
      this.preloaderComplete();
    }
  }
  /**
   * Process user data and save them in the local storage.
   *
   * @private
   * @param {Response} response
   * @memberof PreloaderComponent
   */
  private processUserData(response: Response): void {
    this.subscription.unsubscribe();
    const p: any = response;
    let usr: UserModel = new UserModel(
      this.dataProxyService.getBuc(),
      this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo.cardEmbossName,
      this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo.cardNum,
      this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo
        .cardTrnLimit[0].curAmt.amt,
      this.ccData.cardAcctRelRec[0].cardAcctRelInfo.acctRef.acctRec.acctInfo
        .acctBalances[1].curAmt.amt,
      this.ccData.cardAcctRelRec[0].cardAcctRelInfo.acctRef.acctRec.acctInfo
        .acctBalances[3].curAmt.amt,
      this.ccData.cardAcctRelRec[0].cardAcctRelInfo.acctRef.acctRec.acctInfo.desc,
      this.ccData.cardAcctRelRec[0].cardAcctRelInfo.cardRef.cardRec.cardInfo.closeStmtDt,
      p.acctStmtRec
    );
    this.dataProxyService.setUserData(usr);
    this.preloaderComplete();
  }
  /**
   * Show the error alert.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private showAlert(): void {
    const options: any = {
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    };
    this.modalRef = this.modalService.show(AlertComponent, options);
    this.modalRef.content.type = 'servicesError';
  }
  /**
   * Handle error
   *
   * @private
   * @param {*} error
   * @returns {*}
   * @memberof PreloaderComponent
   */
  private handleError(error: any): any {
    return error;
  }
  /**
   * Preloader complete.
   *
   * @private
   * @memberof PreloaderComponent
   */
  private preloaderComplete(): void {
    this.router.navigate(['welcome']);
  }
}
