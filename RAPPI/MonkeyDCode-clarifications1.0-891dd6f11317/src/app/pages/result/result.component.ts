import { Component, OnInit, OnDestroy, HostListener } from '@angular/core';
import { Router , NavigationEnd } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { DataObject } from './../../shared/data.object';
// Services
import { DataProxyService } from './../../services/data-proxy.service';
import { DataService } from './../../services/data.service';
import { TaggingService } from './../../services/tagging.service';
import { LabelsService } from  './../../services/labels.service';
import { NavigationService } from './../../services/navigation.service';

import { ResponseModel, QuestionsModel } from './../../models/';
import { QualityRatingComponent } from '../../partials/quality-rating/quality-rating.component';

import * as moment from 'moment';
import * as _ from 'lodash';

/**
 *
 *
 * @export
 * @class ResultComponent
 * @implements {OnInit}
 * @implements {OnDestroy}
 */
@Component({
  selector: 'result',
  templateUrl: './result.component.html',
  providers: [
    DataObject,
    DataService,
    DataProxyService,
    NavigationService,
    TaggingService
  ]
})



export class ResultComponent implements OnInit, OnDestroy {
  public questions: QuestionsModel;
  private section: string = 'result';
  private responseModel: ResponseModel;
  private statusCode = 0;
  private rawData: any;
  private errorMessage: string;
  private isEnabled = false;
  private modalRef: BsModalRef;
  

  //Nombre Canal

  public nameChanel : string = '';

  public visaCarta : string = 'false';

  /**
   * Creates an instance of ResultComponent.
   * @param {Router} router
   * @param {DataObject} dataObject
   * @param {DataProxyService} dataProxyService
   * @param {DataService} dataService
   * @param {TaggingService} taggingService
   * @param {LabelsService} labelsService
   * @param {BsModalService} modalService
   * @param {NavigationService} navigationService
   * @memberof ResultComponent
   */
  constructor(
    private router: Router,
    private dataObject: DataObject,
    private dataProxyService: DataProxyService,
    private dataService: DataService,
    private taggingService: TaggingService,
    private labelsService: LabelsService,
    private modalService: BsModalService,
    private navigationService: NavigationService) {
  }

  /**
   * Loads initial content.
   *
   * @memberof ResultComponent
   */
  public ngOnInit() {

    if(this.dataProxyService.getChannel()=='default'){
      this.nameChanel = "SuperMóvil";
    }else{
      this.nameChanel = "SuperWallet";
    }



    // Navigation rules
    // this.navigationService.setTitle('Folio de Aclaración');
    this.navigationService.tapBack('');
    this.navigationService.hideBackButton();

    this.dataService.setUris(this.dataProxyService.getEnviroment());
    this.responseModel = this.dataProxyService.getResponseDAO();
    this.statusCode = this.responseModel.getResult();
    this.questions = this.dataProxyService.getQuestions();
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
          return;
      }
      window.scrollTo(0, 0);
    });
    // GA - Tealium
    const dataLayer: Object = {
      4: this.section,
      17: 'step-result',
    };
    this.taggingService.uTagView(dataLayer);
    this.taggingService.setPageName();
    this.taggingService.send();

    this.visaCarta = this.responseModel.getVisaCard();
  }
  /**
   * Listen the interaction of the user and validate the native session.
   *
   * @private
   * @param {Event} $event
   * @memberof ResultComponent
   */
  @HostListener('window:scroll', ['$event'])
  private onWindowScroll($event: Event): void {
    this.navigationService.validateSession();
  }
  /**
   * Method for the finish button.
   *
   * @memberof ResultComponent
   */
  public finishApp(): void {
    // this.openQualityRatingModal();
    this.navigationService.goToRoot();
  }
  /**
   * Open the Quality Rating modal.
   *
   * @memberof ResultComponent
   */
  public openQualityRatingModal(): void {
    const options: any = {
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true,
      class: 'quality-rating-modal'
    };
    this.modalRef = this.modalService.show(QualityRatingComponent, options);
  }

  /**
   * Method that is called when result component is destroyed.
   *
   * @memberof ResultComponent
   */
  public ngOnDestroy() {
    // TODO: destroy implementation
  }
}
