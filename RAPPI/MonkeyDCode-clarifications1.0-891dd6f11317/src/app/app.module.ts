import 'reflect-metadata';

import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgClass } from '@angular/common';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, ApplicationRef } from '@angular/core';
import { removeNgStyles, createNewHosts, createInputTransfer } from '@angularclass/hmr';
import { RouterModule, PreloadAllModules } from '@angular/router';
import { BarRatingModule } from 'ngx-bar-rating';
import * as moment from 'moment';



/* Platform and Environment providers/directives/pipes */
import { ENV_PROVIDERS } from './environment';
import { ROUTES } from './app.routes';
// App is our top level component
import { AppComponent } from './app.component';
import { APP_RESOLVER_PROVIDERS } from './app.resolver';
import { AppState, InternalStateType } from './app.service';

import { PreloaderComponent, WelcomeComponent,
         QuestionnaireComponent, HistoryComponent,
         SummaryComponent, ResultComponent, NoContentComponent, NoConnectionComponent,
        LockedComponent } from './pages';
//TDD imports components
import {WelcomeTddComponent,QuestionnaireTddComponent,AlertsTddComponent,
        CardTddComponent,FooterTddComponent,SteperTddComponent,TooltipTddComponent,LockedTddComponent,
        ResultTddComponent,SpinnerTddComponent,SummaryTddComponent,PreloaderTddComponent} from './pages/tdd/';

//sTORAGE FOR TDD
import { StorageServiceModule } from 'angular-webstorage-service';
        //partials imports
import { FooterComponent, LoaderComponent,
  AlertComponent, QualityRatingComponent } from './partials';
import { KeysPipe, CustomCurrency, CustomCurrencyPlain, CustomMovesDate, MaskingPan, 
  CurrencySmallCentsModel, ReverseArrayPipe,FormatSmDate,FormatSwDate } from './pipes';

import { TabsModule, ModalModule, BsDropdownModule } from 'ngx-bootstrap';
import { SimpleTimer } from 'ng2-simple-timer';

import { UiSwitchModule } from 'angular2-ui-switch';
import { requestOptionsProvider } from './services/default-request-options.service';
import { ConstantsService } from  './services/constants.service';
import { LabelsService } from  './services/labels.service';
import { DataService } from './services/data.service';
import { DataProxyService } from './services/data-proxy.service';
import { TaggingService } from './services/tagging.service';

import { PersistenceModule } from 'angular-persistence';
/* TDD Services Impots*/
//import { StorageService } from './services/tdd/storage.service';
//import { MainService } from './services/tdd/main.service';
import {UtilsTddService} from './services/tdd/utils-tdd.service'
import {SessionStorageService} from './services/tdd/session-storage.service'
/* Declarations */
import { TextMaskModule } from 'angular2-text-mask';
/* Directives */
import { DebounceClickDirective } from './directives/debounce-click';

import './styles/styles.scss';

/* Alerts */
import { AlertsMain } from './pages/tdd/alerts-tdd/alertsMain'; 

// Application wide providers
const APP_PROVIDERS = [
  ...APP_RESOLVER_PROVIDERS,
  AppState
];

type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};

/**
 * `AppModule` is the main entry point into Angular2's bootstraping process
 */
@NgModule({
  bootstrap: [ AppComponent ],
  declarations: [
    AppComponent,
    PreloaderComponent,
    WelcomeComponent,
    FooterComponent,
    LoaderComponent,
    AlertComponent,
    QualityRatingComponent,
    QuestionnaireComponent,
    SummaryComponent,
    HistoryComponent,
    ResultComponent,
    NoContentComponent,
    NoConnectionComponent,
    LockedComponent,
    CustomCurrency,
    CustomCurrencyPlain,
    CustomMovesDate,
    CurrencySmallCentsModel,
    MaskingPan,
    KeysPipe,
    ReverseArrayPipe,
    FormatSmDate,
    FormatSwDate,
    DebounceClickDirective,
    WelcomeTddComponent,
    QuestionnaireTddComponent,
    AlertsTddComponent,
    CardTddComponent,
    FooterTddComponent,
    SteperTddComponent,
    TooltipTddComponent,
    LockedTddComponent,
    ResultTddComponent,
    SpinnerTddComponent,
    SummaryTddComponent,
    PreloaderTddComponent
  ],
  /**
   * Import Angular's modules.
   */
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    UiSwitchModule,
    BrowserAnimationsModule,
    PersistenceModule,
    TextMaskModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    RouterModule.forRoot(ROUTES, { useHash: true, preloadingStrategy: PreloadAllModules }),
    BarRatingModule,
    StorageServiceModule
  ],
  /**
   * Expose our Services and Providers into Angular's dependency injection.
   */
  providers: [
    ENV_PROVIDERS,
    APP_PROVIDERS,
    requestOptionsProvider,
    SimpleTimer,
    ConstantsService,
    LabelsService,
    UtilsTddService,
    SessionStorageService,
    DataProxyService,
    DataService,
    { provide: 'moment', useValue: moment },
    TaggingService,
    AlertsMain
  ],
  entryComponents: [
    AlertComponent,
    LoaderComponent,
    QualityRatingComponent
  ]
})
export class AppModule {

  constructor(
    public appRef: ApplicationRef,
    public appState: AppState
  ) {}

  public hmrOnInit(store: StoreType) {
    if (!store || !store.state) {
      return;
    }
    /**
     * Set state
     */
    this.appState._state = store.state;
    /**
     * Set input values
     */
    if ('restoreInputValues' in store) {
      let restoreInputValues = store.restoreInputValues;
      setTimeout(restoreInputValues);
    }

    this.appRef.tick();
    delete store.state;
    delete store.restoreInputValues;
  }

  public hmrOnDestroy(store: StoreType) {
    const cmpLocation = this.appRef.components.map((cmp) => cmp.location.nativeElement);
    /**
     * Save state
     */
    const state = this.appState._state;
    store.state = state;
    /**
     * Recreate root elements
     */
    store.disposeOldHosts = createNewHosts(cmpLocation);
    /**
     * Save input values
     */
    store.restoreInputValues  = createInputTransfer();
    /**
     * Remove styles
     */
    removeNgStyles();
  }

  public hmrAfterDestroy(store: StoreType) {
    /**
     * Display new elements
     */
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
