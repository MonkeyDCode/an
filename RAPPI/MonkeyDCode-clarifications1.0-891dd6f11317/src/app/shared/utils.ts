import * as moment from 'moment';
/**
 *some utilities in the app
 *
 * @export
 * @class Utils
 */
export class Utils {

 /**
 *Method that formats a date to shw in the app
 *
 * @param {string} dateToBeParsed
 * @returns
 * @memberof Utils
 */
public retrieveParsedDate(dateToBeParsed: string) {
    moment.locale('es');
    return moment(dateToBeParsed, 'DD-MM-YYYY').format('dddd DD [de] MMMM, YYYY');
  }
}
