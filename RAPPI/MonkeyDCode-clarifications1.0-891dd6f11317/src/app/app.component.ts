import { Component, Injectable } from '@angular/core';

/**
 ** App Component
 */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
@Injectable()
export class AppComponent { }
