const replace = require('replace-in-file');

// Replace base URL
const baseOptions = {
    files: 'dist/index.html',
    // Replacement to make (string or regex) 
    from: /href="\/"/g,
    to: 'href="./"'
};
replace(baseOptions)
  .then((changedFiles) => {
  })
  .catch((error) => {
  });

// Replace assets
const assetsOptions = {
    // Glob(s) 
    files: [
      'dist/**/*.html',
      'dist/**/*.css'
    ],
    // Replacement to make (string or regex) 
    from: /\/assets/g,
    to: 'assets'
};
replace(assetsOptions)
    .then((changedFiles) => {
    })
    .catch((error) => {
    });

// Replace the Tealium script
const tealiumOptions = {
    // Glob(s) 
    files: [
        'dist/**/*.html'
    ],
    // Replacement to make (string or regex) 
    from: /mx\-aclaraciones\/dev/g,
    to: 'mx-aclaraciones/prod'
};
replace(tealiumOptions)
    .then((changedFiles) => {
    })
    .catch((error) => {
    });
