import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-questionary',
  templateUrl: './questionary.component.html'
})
export class QuestionaryComponent implements OnInit {

  private hasCard;
  private commerceInteraction;
  private motiveSelected;
  private motiveSelectedId;
  private motiveClientDescription;
  private validMotiveClientDescription;
  private lostDate;
  private lostDateDay;
  private lostDateMonth;
  private lostDateYear;
  private location;
  private state;
  private days;
  private months;
  private years;
  //8-La reporte como robada o extraviada
  //9-Nunca la tuve conmigo
  //10 = Me la robaron o la extravié y no la he reportado
  private motives=[
    {id:'1',description:'Cargo duplicado',label:'Describa lo que <b>sucedió</b>:',hover:'Ejem.: No reconozco el cargo porque me aparece en más de una ocasión.',tooltip:''},
    {id:'2',description:'Monto alterado',label:'¿Cuál es el importe que <strong>autorizó y reconoce</strong>?',hover:'Ejem.: Es de $120,000.00 el monto que si reconozco.',tooltip:''},
    {id:'3',description:'Cargos adicionales al autorizado',label:'Indique los datos del (los) <strong>consumo(s) que sí reconoce</strong>:',hover:'Ejem.: ID 2429384 - Compra en Liverpool',tooltip:''},
    {id:'4',description:'Pago por otro medio',label:'Describa lo que <strong>sucedió</strong>:',hover:'Ejem.: No hice el pago a través de este medio.',tooltip:''},
    {id:'5',description:'Devolución no aplicada',label:'<strong>Comentarios</strong> adicionales:',hover:'Ejem.: No me han hecho mi devolución.',tooltip:''},
    {id:'6',description:'Mercancias o servicios no proporcionados',label:'<strong>Comentarios</strong> adicionales:',hover:'Ejem.: Nunca me llegó mi mercancía',tooltip:''},
    {id:'7',description:'Cancelación de servicio',label:'<strong>Comentarios</strong> adicionales:',hover:'Ejem.: No estoy de acuerdo con el cobro del servicio, por lo cual lo cancelo.',tooltip:''},
  ]

  states = [
    {"clave": 1, "nombre" : "Aguascalientes"},{"clave": 2, "nombre" : "Baja California"},{"clave": 3, "nombre" : "Baja California Sur"},
    {"clave": 4, "nombre" : "Campeche"},{"clave": 5, "nombre" : "Coahuila"},{"clave": 6, "nombre" : "Colima"},{"clave": 7, "nombre" : "Chiapas"},{"clave": 8, "nombre" : "Chihuahua"},
    {"clave": 9, "nombre" : "Ciudad de Mexico"},{"clave": 10, "nombre" : "Durango"},{"clave": 11, "nombre" : "Guanajuato"},{"clave": 12, "nombre" : "Guerrero"},
    {"clave": 13, "nombre" : "Hidalgo"},{"clave": 14, "nombre" : "Jalisco"},{"clave": 15, "nombre" : "México"},{"clave": 16, "nombre" : "Michoacán"},
    {"clave": 17, "nombre" : "Morelos"},{"clave": 18, "nombre" : "Nayarit"},{"clave": 19, "nombre" : "Nuevo León"},{"clave": 20, "nombre" : "Oaxaca"},
    {"clave": 21, "nombre" : "Puebla"},{"clave": 22, "nombre" : "Querétaro"},{"clave": 23, "nombre" : "Quintana Roo"},{"clave": 24, "nombre" : "San Luis Potosí"},
    {"clave": 25, "nombre" : "Sinaloa"},{"clave": 26, "nombre" : "Sonora"},{"clave": 27, "nombre" : "Tabasco"},{"clave": 28, "nombre" : "Tamaulipas"},
    {"clave": 29, "nombre" : "Tlaxcala"},{"clave": 30, "nombre" : "Veracruz"},{"clave": 31, "nombre" : "Yucatán"},{"clave": 32, "nombre" : "Zacatecas"},{"clave": 33, "nombre" : "Fuera de la Republica"}
  ]

  constructor() {
    this.hasCard='';
    this.resetQuestionary(1);
    this.generateDates();
   }

  ngOnInit() {
  }
  
  /**
  * Method to go botoom page
  *
  * 
  */
  private scrollBottom(){
    setTimeout(()=>{
      window.scrollTo(0,document.body.scrollHeight);
    },150)
    
  }

  /**
  * Method that generates the values of the date
  *
  * 
  */
  generateDates(){
    this.days =['01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
    this.months=['ENE','FEB','MAR','ABR','MAY','JUN','JUL','AGO','SEP','OCT','NOV','DIC'];
    let date = moment(); 
    this.years=[date.format('YYYY')];
    for(let i=0;i<10;i++){
      this.years.push(date.subtract(1,'years').format('YYYY'));
    }  
  }


  /**
  * Input validator for the comments
  *
  * @param event {any}
  */
  inputValidator(event: any) {
    const pattern = /^[a-zA-Z0-9 _\\-\\.:,;áéíóúÁÉÍÓÚÜü¿?"¡!#$%&()=]*$/;   
    if (!pattern.test(event.target.value)) {
      event.target.value = event.target.value.replace(/[^a-zA-Z0-9 _\\-\\.:,;áéíóúÁÉÍÓÚÜü¿?"¡!#$%&()=]/g, "");
    }
  }

  validateMotiveDescription(){
    this.motiveClientDescription.length >=10 ? this.validMotiveClientDescription = true: this.validMotiveClientDescription = false;
  }

  /**
  * set the correct value to motiveSelected when changes the motive
  *
  * @param event {any}
  */
  changeMotive(id){
    if(Number(id) === 8 || Number(id) === 9 ||Number(id) === 10 ){
      this.motiveSelected = {id:id };
    }else{
      this.motiveSelected = this.motives[Number(id)-1];
    }
  }

  /**
  * reset the value of the questionary depending the level of reset
  *
  * @param level {int} 
  */
  resetQuestionary(level){
    switch(level){
      case 1:
        this.commerceInteraction='';
        this.motiveSelectedId='0';
        this.motiveSelected={id:'0'}
        this.motiveClientDescription='';
        this.validMotiveClientDescription=false;
        this.lostDate='';
        this.lostDateDay='';
        this.lostDateMonth='';
        this.lostDateYear='';
        this.state='';
        this.location='';
        break;
      case 2:
        this.motiveSelectedId='0';
        this.motiveSelected={id:'0'}
        this.motiveClientDescription='';
        this.validMotiveClientDescription=false;
        this.lostDate='';
        this.lostDateDay='';
        this.lostDateMonth='';
        this.lostDateYear='';
        this.state='';
        this.location='';
        break;
      case 3:
        this.motiveClientDescription='';
        this.validMotiveClientDescription=false;
        this.lostDate=''; 
        this.lostDateDay='';
        this.lostDateMonth='';
        this.lostDateYear='';
        this.state='';
        this.location='';
        break;
      case 4:
        this.location='';
        this.state='';
        break;
      case 5:
        this.state='';
        break;
      case 6:
        this.validMotiveClientDescription=false;
        break;
      default:
        break;
    }
  }

  /**
   * Check if the lost date is valid
   *
   */
  public validateDate() {
    if(this.lostDateDay!=='' && this.lostDateMonth!=='' && this.lostDateYear!==''){
      if(moment(`${this.lostDateDay}/${this.lostDateMonth}/${this.lostDateYear}`,'DD/MMM/YYYY','es').isValid()){
        this.lostDate = moment(`${this.lostDateDay}/${this.lostDateMonth}/${this.lostDateYear}`,'DD/MMM/YYYY','es').format('DD-MMM-YYYY');
      }
    }
  }

  /**
   * generates the multifolio object 
   *
   */
  validateForm(){

  }
}
