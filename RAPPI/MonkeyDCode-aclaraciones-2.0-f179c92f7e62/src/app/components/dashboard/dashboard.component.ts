import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router, ActivatedRoute, ParamMap, Route } from '@angular/router';

// Services
import { StorageService } from '../../services/local-storage.service';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html'
})

export class DashboardComponent implements OnInit {

  //SETEO VARIABLES

  // Token
  token : string;
  
  //Spinner
  spinner: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private storage: StorageService,
    private mainService: MainService
  ) {

    //DEC VARIBALES

    //Spinner
    this.spinner = true;

    //GET URL PARAMETERS
    route.paramMap.subscribe(params => {
      this.token = params.get('token');
      this.storage.set('token',this.token);

    });

  }

  ngOnInit() {

    this.saveSession();
  }

  //GET CLIENT ID

  private saveSession(): void {
    // session
    if (this.storage.get('token')) {
      this.spinner=false;
    }else{
      this.spinner=true;
    }
  }

  //ADD ACTIVE LI
  clickMove(id){
    let element = document.getElementById(id);

    if(element.classList.contains("active")){
      element.classList.remove("active");
    }else{
      element.classList.add("active");
    }
  }


  //SHOW TOGGLE
  showToggle(id){
    let element = document.getElementById(id);
    element.classList.add("animate");
  }

  //HIDE TOGGLE
  hideToggle(id){
    let element = document.getElementById(id);
    element.classList.remove("animate");
    element.classList.add("animateReverse");
    setTimeout(function(){
      element.classList.remove("animateReverse");
    }, 700);
  }

}
