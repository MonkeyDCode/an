import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LocalStorageModule } from 'angular-2-local-storage';

// Router
import { RoutingModule } from './modules/routing/routing.module';

// Components
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { QuestionaryComponent } from './components/questionary/questionary.component';
import { SummaryComponent } from './components/summary/summary.component';
import { ResultComponent } from './components/result/result.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { SteperComponent } from './components/generics/steper/steper.component';
import { CardComponent } from './components/generics/card/card.component';
import { FooterComponent } from './components/generics/footer/footer.component';
import { LockedComponent } from './components/locked/locked.component';


// service 
import { StorageService } from './services/local-storage.service';
import { MainService } from './services/main.service';
import { AlertsComponent } from './components/alerts/alerts.component';



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    QuestionaryComponent,
    SummaryComponent,
    ResultComponent,
    SpinnerComponent,
    SteperComponent,
    CardComponent,
    FooterComponent,
    LockedComponent,
    AlertsComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RoutingModule,
    HttpClientModule,
    LocalStorageModule.withConfig({
      prefix: 'webresponsive',
      storageType: 'localStorage'
    })
  ],
  providers: [
    StorageService,
    MainService    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
