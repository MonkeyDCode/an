import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// Components 
import { DashboardComponent } from '../../components/dashboard/dashboard.component';
import { QuestionaryComponent } from '../../components/questionary/questionary.component';
import { SummaryComponent } from '../../components/summary/summary.component';
import { ResultComponent } from '../../components/result/result.component';
import { LockedComponent } from '../../components/locked/locked.component';



const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'dashboard/:token',
    component: DashboardComponent
  },
  {
    path: 'questionary',
    component: QuestionaryComponent
  },
  {
    path: 'summary',
    component: SummaryComponent
  },
  {
    path: 'result',
    component: ResultComponent
  },
  {
    path: 'locked',
    component: LockedComponent
  },
  {
    path: '**',
    redirectTo: 'dashboard'
  } 
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class RoutingModule { }
