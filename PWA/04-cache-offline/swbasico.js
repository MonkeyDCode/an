
self.addEventListener('fetch',event =>{
    const offlineResponse = new Response(`
        Bienvenido, necesitas internet :s
    `);

    const offPage = fetch('pages/offline.html')
    const resp = fetch(event.request)
                    .catch(()=>{
                        return offPage;
                    })

    event.respondWith(resp);



});

