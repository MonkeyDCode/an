
// Ciclo de vida del SW
//CUANDO SE INSTALA
self.addEventListener('install',event => {
    //DESCARGAR ASSETS
    // CREAR CACHE
    //ACTIVAR EL SW EN CUANTO SE INSTALA
    //self.skipWaiting();

    //ESPERAR HASTA QUE SE LLENE EL CACHE
    const instalacion = new Promise((resolve,reject) => {
        setTimeout(()=>{
            self.skipWaiting();
            resolve();
        },1)
    })
    event.waitUntil(instalacion);
})

//CUANDO SE ACTIVA
self.addEventListener('activate',event => {
    //DESCARGAR ASSETS
    // BORRAR CACHE VIEJO
});


//EVENTO FETCH 
self.addEventListener('fetch',event => {
    //APLICAR ESTRATEGIAS DE CACHE

    //modificar la respuesta
});



//RECUPERAMOS LA CONEXION A INTERNET
self.addEventListener('sync', event => {
    console.log('tenemos conexion');
    console.log(event);
    console.log(event.tag)
});


//PUSH NOTIFICATIONS
self.addEventListener('push',event =>{
    console.log('Notificación recibida')
})