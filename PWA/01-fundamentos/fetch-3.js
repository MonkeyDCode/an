//URL 'https://reqres.in/api/users'

fetch('https://reqres.in/api/users',{
    method:'POST',
    body:JSON.stringify({
        nombre:'Name',
        edad: 24
    }),
    headers:{
        'Content-Type':'application/json'
    }
})
        .then(resp =>resp.json())
        .then(resp =>{
            console.log(resp)
        }
    )
    .catch(error =>{
        console.log('Error')
    })