function sumarUno(numero,callback){

    let promesa = new Promise(function(resolve,reject){
        if(numero >= 7){
            reject('El numero es muy alto')
        }
        setTimeout(function(){
            resolve(numero+1);
        },800);

    });
    
    
    return promesa;
}


sumarUno(5).then( nuevoValor => sumarUno(nuevoValor))
            .then( sumarUno)
            .then(nuevoValor =>{
                return sumarUno(nuevoValor);
            })
            .then(nuevoValor =>{
                console.log(nuevoValor);
            })
            .catch(error =>{
                console.log('ERROR');
                console.log(error);
            })