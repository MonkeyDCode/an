function sumarUno(numero,callback){
    if(numero>=7){
        callback('numero muy alto');
        return;
    }

    setTimeout(function(){
        callback(null,numero+1);
    },800)
    
}
sumarUno(5,function(error,nuevoVal){
    if(error){
        console.log(error);
        return;
    }
    sumarUno(nuevoVal,function(error,nuevoVal2){
        if(error){
            console.log(error);
            return;
        }
        sumarUno(nuevoVal2,function(error,nuevoVal3){
            if(error){
                console.log(error);
                return;
            }
            console.log(nuevoVal3)
        })
    })
});
